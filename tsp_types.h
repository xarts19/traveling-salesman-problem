#pragma once

#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <cmath>
#include <exception>

//typedef int integer;
//typedef unsigned int uinteger;

inline void* safe_malloc(size_t size)
{
    void* mem = malloc(size);
    if (!mem)
    {
        printf("No free memory!!!!! (%u required)\n", size);
        throw std::exception();
    }
    return mem;
}
/*
double inline __declspec (naked) __fastcall sqrt14(double n)
{
    _asm fld qword ptr [esp+4]
    _asm fsqrt
    _asm ret 8
}
*/
inline int nint(double fl)
{
    return static_cast<int>(fl + 0.5);
}

struct IntVector
{
    static IntVector*   alloc(unsigned int len)
    {
        assert(len > 0);
        IntVector* v = static_cast<IntVector*>( safe_malloc( sizeof(IntVector) + (len - 1) * sizeof(int) ) );
        v->size = len;
        return v;
    }

    static void         dealloc(IntVector* instance)            { free(instance); }

    inline int          type_size() const                       { return sizeof(int); }
    inline int          at(unsigned int index) const            { assert( index < size ); return vector[index]; }
    inline int*         pointer_at(unsigned int index)          { assert( index < size ); return vector + index; }
    inline const int*   pointer_at(unsigned int index) const    { assert( index < size ); return vector + index; }

    inline void         fill(int value)
    {
        for ( unsigned int i = 0; i < size; ++i ) { *(pointer_at(i)) = value;}
    }

    inline IntVector*   copy() const
    {
        IntVector* v = static_cast<IntVector*>( safe_malloc( sizeof(IntVector) + (size - 1) * type_size() ) );
        v->size = size;
        memcpy( v->vector, vector, type_size() * size );
        return v;
    }

    inline IntVector*   copy_segment(unsigned int start, unsigned int end) const
    {
        assert(start < end && end <= size);
        int len = end - start;
        IntVector* v = static_cast<IntVector*>( safe_malloc( sizeof(IntVector) + (len - 1) * type_size() ) );
        memcpy( v->vector, vector + start, type_size() * len );
        v->size = len;
        return v;
    }

    inline void reverse_subarray( unsigned int start, unsigned int end )
    {
        assert( start < end && end <= size );

        for ( unsigned int i = 0; i < (end - start) / 2; ++i )
        {
            swap(start + i, end - i - 1);
        }
    }

    inline void move_subarray( unsigned int start, unsigned int new_start, unsigned int len )
    {
        assert(start + len <= size && new_start + len <= size);
        memmove(pointer_at(new_start), pointer_at(start), len * type_size());
    }

    unsigned int find(int value, unsigned int start = 0)
    {
        for (unsigned int i = start; i < size; ++i)
            if (at(i) == value) return i;
        assert(0 && "Value not found in IntVaector");
        return (unsigned int)-1;
    }

    inline void swap(unsigned int i, unsigned int j)
    {
        assert( i < size && j < size );
        int tmp = at(i);
        *(pointer_at(i)) = at(j);
        *(pointer_at(j)) = tmp;
    }

    inline void print() const
    {
        for (unsigned int i = 0; i < size; ++i)
            printf("%d ", vector[i]);
        printf("\n");
    }

    static long long memory(unsigned int size) { return (sizeof(int) * size + sizeof(size) ); }

public:
    unsigned int size;
    int vector[1];
    //var sized
};

inline void slice_copy( const IntVector* source,
                       IntVector* destination,
                       unsigned int source_start,
                       unsigned int dest_start,
                       unsigned int length )
{
    assert( source_start + length <= source->size && 
            dest_start   + length <= destination->size );

    memcpy( destination->pointer_at(dest_start), 
            source->pointer_at(source_start), 
            source->type_size() * length );
}

inline void slice_copy_reversed( const IntVector* source,
                                IntVector* destination,
                                unsigned int source_start,
                                unsigned int dest_start,
                                unsigned int length )
{
    assert(source_start + length <= source->size && 
             dest_start + length <= destination->size);

    for ( unsigned int i = 0; i < length; ++i )
    {
        *( destination->pointer_at( dest_start + i ) ) = 
            source->at( source_start + (length - 1) - i );
    }
}

template <typename T>
struct Matrix
{
    static Matrix<T>* try_alloc(unsigned int size)
    {
        Matrix* m = static_cast<Matrix<T>*>( malloc( sizeof(Matrix<T>) + (size * size - 1) * sizeof(T) ) );
        if (m != NULL)
            m->size = size;

        return m;
    }

    static Matrix<T>* alloc(unsigned int size)
    {
        Matrix<T>* m = static_cast<Matrix<T>*>( safe_malloc( sizeof(Matrix<T>) + (size * size - 1) * sizeof(T) ) );
        m->size = size;
        return m;
    }

    static void dealloc(Matrix<T>* instance) { free(instance); }

    inline T at( unsigned int row, unsigned int column ) const
    {
        assert( row < size && column < size );
        return matrix[ ( row * size ) + column ];
    }

    inline T* pointer_at(unsigned int row, unsigned int column)
    {
        assert( row < size && column < size );
        return matrix + ( row * size ) + column;
    }

    inline const T* pointer_at(unsigned int row, unsigned int column) const
    {
        assert( row < size && column < size );
        return matrix + ( row * size ) + column;
    }

    inline T& to( unsigned int row, unsigned int column )
    {
        assert( row < size && column < size );
        return matrix[ ( row * size ) + column ];
    }

    inline Matrix<T>* copy() const
    {
        Matrix<T>* m = static_cast<Matrix<T>*>( safe_malloc( sizeof(Matrix<T>) + (size * size - 1) * sizeof(T) ) );
        m->size = size;
        memcpy( m->matrix, matrix, sizeof(T) * size * size );
        return m;
    }

    inline void set_to_zero()
    {
        memset( matrix, 0, sizeof(T) * size * size );
    }

    inline void fill(T value)
    {
        for (unsigned int i = 0; i < size; ++i)
            for (unsigned int j = 0; j < size; ++j)
                matrix[ ( i * size ) + j ] = value;
    }

    inline long long consumed_memory() const { return (sizeof(T) * size * size + sizeof(size) ); }

    static long long memory_for_matrix(unsigned int size) { return (sizeof(T) * size * size + sizeof(size) ); }

public:
    unsigned int size;
    T matrix[1];
    //var sized
};

typedef Matrix<int> IntMatrix;
typedef Matrix<float> FloatMatrix;

struct IntMatrList
{
    static void dealloc(IntMatrList* instance) { free(instance); }

    inline int at( unsigned int row, unsigned int column ) const
    {
        assert( row < size && column < size );
        
        double dx = matrix[row].x - matrix[column].x;
        double dy = matrix[row].y - matrix[column].y;
        //dx *= dx;
        //dy *= dy;
        double distance = sqrt(dx * dx + dy * dy);
        //return nint( sqrt14( dx + dy ) );
        return nint(distance);
    }

    inline IntMatrList* copy() const
    {
        IntMatrList* m = static_cast<IntMatrList*>( safe_malloc( sizeof(IntMatrList) + (size * 2 - 1) * sizeof(double) ) );
        m->size = size;
        memcpy( m->matrix, matrix, sizeof(double) * 2 * size );
        return m;
    }

    inline long long consumed_memory() const { return (sizeof(double) * 2 * size + sizeof(size) ); }

    static long long memory_for_matrix(unsigned int size) { return (sizeof(double) * 2 * size + sizeof(size) ); }

public:
    unsigned int size;
    struct {
        double x;
        double y;
    } matrix[1];
    //var sized
};
