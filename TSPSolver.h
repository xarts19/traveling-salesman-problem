#pragma once

#include "tsp_types.h"
#include "TSPNeighborhood.h"
#include "TSPGenetic.h"
#include "TSPAntColony.h"
#include "common.h"

#include "VTUtils/VTCPPLogger.h"
#include "VTUtils/VTTimer.h"

#include <vector>
#include <algorithm>
#include <cmath>
#include <string>
#include <map>
#include <limits>

#include <assert.h>
#include <time.h>

namespace TSP
{
    template <typename T_DistancesType, typename T_PathType>
    long long calc_cost(const T_DistancesType* distances, const T_PathType* path)
    {
        long long cost = 0;

        for ( unsigned int i = 0; i < path->size - 1; i++ )
        {
            cost += distances->at( path->at(i), path->at(i + 1) );
        }

        cost += distances->at(  path->at(path->size - 1),  path->at(0) );

        return cost;
    }

    template <typename T_PathType>
    void random_path( unsigned int number_of_cities, T_PathType* path )
    {
        assert(path->size == number_of_cities);

        // not performance-critical, so we can use vector
        std::vector<int> cities;
        cities.reserve(number_of_cities);

        for (unsigned int i = 0; i < number_of_cities; ++i)
            cities.push_back(i);

        std::random_shuffle(cities.begin(), cities.end());

        for (unsigned int i = 0; i < number_of_cities; ++i)
        {
            *(path->pointer_at(i)) = cities[i];
        }
    }

    class IAlgOptions
    {
    public:
        template <typename T>
        void setopt         (const char* name, T value)      { assert(0 && "Option type not supported"); }
        
        template <typename T>
        T getopt             (const char* name) const { assert(0 && "Option type not supported"); }

        template <typename T>
        bool hasopt         (const char* name) const { assert(0 && "Option type not supported"); }

        inline void clear() { int_options.clear(); double_options.clear(); bool_options.clear(); }

    private:
        std::map<std::string, int>    int_options;
        std::map<std::string, double> double_options;
        std::map<std::string, bool>   bool_options;
    };
    
    template <>
    inline void IAlgOptions::setopt<int>    (const char* name, int value)    { int_options[name] = value; }
    template <>
    inline void IAlgOptions::setopt<double> (const char* name, double value) { double_options[name] = value; }
    template <>
    inline void IAlgOptions::setopt<bool>   (const char* name, bool value)   { bool_options[name] = value; }

    template <>
    inline int IAlgOptions::getopt<int>      (const char* name) const { return int_options.find(name)->second; }
    template <>
    inline double IAlgOptions::getopt<double>(const char* name) const { return double_options.find(name)->second; }
    template <>
    inline bool IAlgOptions::getopt<bool>    (const char* name) const { return bool_options.find(name)->second; }

    template <>
    inline bool IAlgOptions::hasopt<int>    (const char* name) const { return (int_options.count(name) >= 1); }
    template <>
    inline bool IAlgOptions::hasopt<double> (const char* name) const { return (double_options.count(name) >= 1); }
    template <>
    inline bool IAlgOptions::hasopt<bool>   (const char* name) const { return (bool_options.count(name) >= 1); }

    class IteratedLocalSearchOptions : public IAlgOptions
    {
    public:
        inline int iterations() const { return hasopt<int>("iterations") ? getopt<int>("iterations") : 200; }
    };

    class VaryingNeighborhoodSearchOptions : public IAlgOptions
    {
    public:
        inline bool skip_5opt() const { return hasopt<bool>("skip_5opt") ? getopt<bool>("skip_5opt") : false; }
    };


    /* Traveling salesman problem solver */
    template <typename T_Container>
    class Solver
    {
    public:
        ContainerType container_type;
        Algorithms algorithm;

    private:
                EdgeType     _type;
                unsigned int _number_of_cities;
                bool         logging_;
		mutable VT::Logger   _logger;
                IntVector*   _initial_approximation;
                bool         _use_init_approx;
                IAlgOptions  _options;
        mutable VT::Timer    _timer;

        T_Container* _distance_matrix;

    public:
        Solver( unsigned int number_of_cities, T_Container* distance_matrix, ContainerType cont_type, Algorithms alg ) :
            container_type(cont_type),
            algorithm(alg),
            _type(Symmetric),
            _number_of_cities( number_of_cities ),
			_logger("default"),
            _initial_approximation( NULL ),
            _use_init_approx(false),
            _options(),
            _timer(false),
            _distance_matrix( distance_matrix )
        {
            /* initialize random seed: */
            srand( static_cast<unsigned int>( time(NULL) ) );

            _initial_approximation = IntVector::alloc(_number_of_cities);
        }

        ~Solver() { IntVector::dealloc(_initial_approximation); T_Container::dealloc(_distance_matrix); }

        inline bool is_symmetric() const { return _type == Symmetric; }

        /* solver options */
        void set_logging(VT::LogLevel level) { logger().set_cout(level); }
		VT::Logger& logger() const { return _logger;  }

        void set_seed(int seed)
        {
            logger().debug() << "Seed set to " << seed;
            srand( static_cast<unsigned int>(seed) );
        }

        void set_edge_type(EdgeType t) { _type = t; }

        void set_initial_approximation( const IntVector* path )
        {
            assert(path->size == _number_of_cities);
            slice_copy( path, _initial_approximation, 0, 0, _number_of_cities );
            _use_init_approx = true;
        }

        void start_timer() { _timer.start(); }

        inline void disable_init_approx() { _use_init_approx = false; }

        inline const IAlgOptions& options() const { return _options; }
        inline IAlgOptions& options() { return _options; }

    private:
        inline const IteratedLocalSearchOptions& options_ILS() const { return static_cast<const IteratedLocalSearchOptions&>(options()); }
        inline const VaryingNeighborhoodSearchOptions& options_VNS() const { return static_cast<const VaryingNeighborhoodSearchOptions&>(options()); }

    /* solver algorithms */
    public:
        int local_search( IntVector* result, long long& cost, bool silent = false ) const;
        
        
        /*
              Option name                Type          Default
            init_probability_level      double           10.0
            iterations                   int           1000000
            final_probability_level     double          0.001
        */
        int simulated_annealing( IntVector* result, long long& cost );
        
        
        int varying_nbh_search( IntVector* result, long long& cost, bool silent = false ) const;
        
        
        int iterated_local_search( IntVector* result, long long& cost );

        int guided_local_search( IntVector* result, long long& cost );

        int genetic_algorithm( IntVector* result, long long& cost );
        int ant_colony( IntVector* result, long long& cost );

        // G-alg; G search; GS
        /*
              Option name                Type          Default
            init_probability_level      double           0.5
            iterations                   int            10000
            final_probability_level     double          1 - 0.0000001
        */
        int gs_algorithm( IntVector* result, long long& cost );

        int h_algorithm( IntVector* result, long long& cost );

        enum VNS_Stage
        {
            VNS_Stage_2opt,
            VNS_Stage_3opt,
            VNS_Stage_4opt,
            VNS_Stage_5opt
        };

    private:

        template <typename T_Nbh>
        int local_search_worker( IntVector* result, long long& cost, bool silent = false ) const;

        template <typename T_Nbh, typename T_Plugin>
        int probabilistic_modeling( IntVector* result, long long& cost );

        template <typename T_Nbh>
        int simulated_annealing_worker( IntVector* result, long long& cost );
        
        // return true on improvement and false, if all is scanned and no improvement found
        template <typename T_Nbh>
        bool varying_nbh_search_worker( T_Nbh nbh, long long& cost, bool stop_on_improvement ) const;

        template <typename T_Nbh>
        int gs_algorithm_worker( IntVector* result, long long& cost, int iterations = 1000000 );

        template <typename T_Model>
        bool populational_alg_template( IntVector* result, long long& cost, int steps );

        IntVector* create_initial_path() const;

        // return default time to run for this problem type and size
        int get_time_constraints(unsigned int size) const;
		bool time_is_out(int time_limit) const;

    };
};
