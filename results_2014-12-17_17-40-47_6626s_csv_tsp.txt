Name,Size,Type,Exact,GA,Result,Time,q%
problems/bays29.tsp,29,TSP,2020,      ,2020,0.516,0.00
problems/swiss42.tsp,42,TSP,1273,      ,1273,1.307,0.00
problems/att48.tsp,48,TSP,10628,      ,10628,1.783,0.00
problems/eil51.tsp,51,TSP,426,      ,426,2.012,0.00
problems/berlin52.tsp,52,TSP,7542,      ,7542,2.014,0.00
problems/eil76.tsp,76,TSP,538,      ,538,5.081,0.00
problems/eil101.tsp,101,TSP,629,      ,630,10.548,0.16
problems/d493.tsp,493,TSP,35277,      ,35277,1237.308,0.00
problems/att532.tsp,532,TSP,27914,      ,27914,1697.176,0.00
problems/u574.tsp,574,TSP,37409,      ,37409,1397.971,0.00
problems/p654.tsp,654,TSP,34680,      ,34680,1344.003,0.00
problems/d657.tsp,657,TSP,49333,      ,49333,2651.337,0.00
problems/u724.tsp,724,TSP,42256,      ,42256,3969.415,0.00
problems/rat783.tsp,783,TSP,8936,      ,8936,3714.603,0.00
