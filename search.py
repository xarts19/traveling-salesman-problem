#!/usr/bin/env python

"""Auto-generated file

"""

__author__ = "Xarts19 (xarts19@gmail.com)"
__version__ = "Version: 0.0.1 "
__date__ = "Date: 2011-10-22 11:44:15.005799 "

import os
import sys
import logging
import math
#import decorators
import time

from multiprocessing import Process, Queue

from math import sqrt

import tsp_cpp_algs
from options import Algorithms

LOGGER = logging.getLogger('main.search')
MAX_DIST = 1000000

class ParseException(Exception):
     def __init__(self, value):
         self.value = value
     def __str__(self):
         return repr(self.value)

def nint(floating):
    '''Round to nearest int, or to higher in middle case'''
    return int(floating + 0.5)
    
class Solver(object):

    def __init__(self, options):
        self.options = options
        self.current_set = set()
        self.total_files_count = len(options.files)
        self.processed_files_count = 0
        
        # thread-safe queue for accumulating results
        self.queue = Queue()


    def solve(self, solutions):
        '''Given list of files solves problems with method and prints results.'''

        # list of scheduled calculations
        processes = []
        
        if self.options.method not in Algorithms:
            raise Exception("No such method: %s" % self.options.method)
        
        try:
                
            for filename in self.options.files:

                try:
                    parser = ProblemInputParser(filename, self.options.initials_extension)
                    
                    try:
                        problem = parser.parse()
                    except ParseException as e:
                        self.total_files_count -= 1
                        LOGGER.error( "Failed to parse file: %s;  Error: %s" % (filename, e.value) )
                        continue
                        
                    if (problem.size > self.options.filter_size):
                        self.total_files_count -= 1
                        LOGGER.info( "Problem size (%d) larger then filter (%d)" % (problem.size, self.options.filter_size) )
                        continue
                    
                    # results object for current problem
                    solutions[filename] = Result(problem)
                    self.current_set.add(filename)

                    init = problem.get_initial_approximations()
                    if init == None:
                        init = [None] * self.options.restarts
                    self.options.restarts = len(init)
                        
                    # for each initial vector
                    for i_a in init:
                        proc = Process(target=process_fnc, args=(self, filename, problem, i_a))
                        processes.append(proc)
                        proc.start()
                        
                        if self.options.progress:
                            self.print_progress()

                        if len(processes) >= self.options.multiproc:
                            self.get_next_result(processes, solutions)
                                    
                    if len(processes) >= self.options.multiproc:
                        self.get_next_result(processes, solutions)

                except IOError as e:
                    LOGGER.error("%s" % e)
                    solutions[filename] = None

            while len(processes) > 0:
                self.get_next_result(processes, solutions)
                        
        except (KeyboardInterrupt, SystemExit):
            
            Logger.info("Interrupt, termination all processes: %s" % processes)
            for proc in processes:
                if proc is not None:
                    print("Terminating " + str(proc))
                    proc.terminate()
                    proc.join()
                                
        if self.options.progress:
            sys.stderr.write("\n")
            sys.stderr.flush()

    def get_next_result(self, processes, solutions):
        LOGGER.debug("Starting to wait for queue, current processes: %s" % processes)
        pid, result = self.queue.get()
        LOGGER.debug("Got queue item from pid %s" % str(pid))
        
        if result == None:
            raise KeyboardInterrupt
        
        self.save_result(solutions, result)
        for proc in processes:
            if proc.pid == pid:
                proc.join()
                processes.remove(proc)
                LOGGER.debug("Removed process %s, current processes: %s" % (pid, processes))
                break

    def save_result(self, solutions, result):
        filename, restarts, i_a, solution, cost, time = result
            
        solutions[filename].add(solution, i_a, cost)
        
        LOGGER.debug("Solution: %s" % solution)
        LOGGER.debug("Cost: %s" % cost)
        
        if len(solutions[filename].results) == restarts:
            # save running time
            solutions[filename].add_time(time)
            self.current_set.remove(filename)
            solution, cost = solutions[filename].get_best()
            LOGGER.debug("Best solution: %s" % solution)
            LOGGER.debug("Best cost: %s" % cost)
            self.processed_files_count += 1
            if self.options.progress:
                self.print_progress()


    def print_progress(self):
        total_size = 25
        percent = 100.0 * self.processed_files_count / self.total_files_count
        completed = "=" * int(total_size * percent / 100.0)
        noncompleted = " " * (total_size - len(completed) - 1)
        set_str = '[' + ", ".join(map(lambda x: x.rpartition("/")[2], self.current_set)) + ']'
        max_len = 55
        out = "\r [%s>%s] %.2f%% %d/%d %s" % (completed, noncompleted, percent, self.processed_files_count, self.total_files_count, set_str)
        out += " " * (max_len - len(set_str))
        sys.stderr.write(out)
        sys.stderr.flush()


def process_worker(options, problem, i_a):
    return tsp_cpp_algs.run_algorithm(options, problem.type, problem.distances, problem.hasMatrix, i_a)

def process_fnc(solver, filename, problem, i_a):
    # solve the problem
    try:
        start = time.time()
        solution, cost = process_worker(solver.options, problem, i_a)
        LOGGER.debug("Processing %s done" % filename)
        solver.queue.put((os.getpid(), [filename, solver.options.restarts, i_a, list(solution), cost, time.time() - start]))
        LOGGER.debug("Result for %s added to queue" % filename)
    except KeyboardInterrupt:
        solver.queue.put((os.getpid(), None))


class Problem(object):
    '''Travelling salesman problem. Distances is a hash table from pair of cities to distance
    between them.
    '''

    def __init__(self, type, name, size, distances_matrix=None, coord_vector=None, initial_approximations=None):
        self.type = type  # "TSP" or "ATSP"
        self.name = name
        self.size = size
        
        if (distances_matrix and not coord_vector):
            self.distances = distances_matrix
            self.hasMatrix = True
        elif (coord_vector and not distances_matrix):
            self.distances = coord_vector
            self.hasMatrix = False
        else:
            assert(False and "Should be either matrix or vector")
            
        self.initial_approximations = initial_approximations

    def __repr__(self):
        return "<%s problem: %s; Size: %s\nDistances: %s>" % \
            (self.__class__, self.name, str(self.size), str(self.distances))

    def __str__(self):
        return "<%s problem: %s; Size: %s>" % \
            (self.__class__, self.name, str(self.size))

    def get_initial_approximations(self):
        return self.initial_approximations

class Result(object):
    '''Results are tuples : (solution, initial, time)'''

    def __init__(self, problem):
        self.results = []
        self.problem = problem
        self.time = 0           # this is single-core total time for all restarts, in seconds
        self.size = problem.size

    def get_best(self):
        #solution, i_a, cost = self.results[0]
        best_index = min( range(len(self.results)), key=lambda i : self.results[i][2] )
        return self.results[best_index][0], self.results[best_index][2]

    def get_best_initial(self):
        # removed while refactoring
        """
        inits = self.problem.get_initial_approximations()
        best = inits[0]
        best_cost = self.problem.calc_cost(best)
        for init in inits:
            cost = self.problem.calc_cost(init)
            if cost < best_cost:
                best = init
                best_cost = cost
        return best, best_cost
        """
        return [], -1

    def add_time(self, t):
        self.time += t

    def add(self, solution, initial, cost):
        self.results.append((solution, initial, cost))


class ProblemInputParser(object):
    '''Reads TSPLIB file format.'''

    def __init__(self, filename, initial_approximation_ext):
        self.filename = filename
        self.name = None
        self.type = None
        self.dimension = None
        self.edge_weight_type = None
        self.edge_weight_format = None
        self.data = ""
        self.initial_approximation_ext = initial_approximation_ext


    def parse(self):
        '''Process given file.'''
        content = [line for line in open(self.filename)]
        line_num = 0
        while True:
            line = content[line_num]
            # save parameters
            if ':' in line:
                param_name, param_value = line.split(':')
                param_name = param_name.strip()
                param_value = param_value.strip()
                self._set_param(param_name, param_value)
            
            elif 'EDGE_WEIGHT_SECTION' in line or 'NODE_COORD_SECTION' in line:
            
                # read whole data section and store it in one big string
                start_line = line_num + 1
                end_line = line_num + 1
                while True:
                    end_line += 1
                    if end_line >= len(content):
                        break
                    if 'SECTION' in content[end_line] or 'EOF' in content[end_line]:
                        break
                
                self.data = ''.join(content[start_line:end_line])
                    
            line_num += 1
            if line_num >= len(content):
                break

        distances = self._get_distance_table()
        if distances:
            return Problem(self.type,
                           self.name,
                           self.dimension,
                           distances_matrix = distances,
                           initial_approximations = self._get_initial_approximations())
        else:
            return Problem(self.type,
                           self.name,
                           self.dimension,
                           coord_vector = self._get_distance_vector(),
                           initial_approximations = self._get_initial_approximations())


    def _set_param(self, name, value):
        if name == 'NAME':
            self.name = value
        elif name == 'TYPE':
            self.type = value
        elif name == 'DIMENSION':
            self.dimension = int(value)
        elif name == 'EDGE_WEIGHT_TYPE':
            self.edge_weight_type = value
        elif name == 'EDGE_WEIGHT_FORMAT':
            self.edge_weight_format = value
        elif name == 'COMMENT':
            pass
        else:
            LOGGER.info("unknown parameter: \'%s\' with value \'%s\'" % (name, value))

    def _get_initial_approximations(self):
        if self.initial_approximation_ext:
            try:
                ia_file = open(os.path.splitext(self.filename)[0] + self.initial_approximation_ext)
                return [[int(v) for v in line.strip().split() if v] for line in ia_file if line]
            except IOError:
                return None
        else:
            return None

    def _get_distance_table(self):
        if self.edge_weight_type == 'EXPLICIT':
            if self.edge_weight_format == 'FULL_MATRIX':
                return self._matrix_parse()
            else:
                raise ParseException("edge_weight_format not implemeted")
                
        elif self.dimension < 1000:
            return self._node_coord_parse()
                
        elif self.dimension > 1000 and self.edge_weight_type == 'EUC_2D':
            return None
        
        else:
            raise ParseException("edge_weight_type not implemented")

    def _matrix_parse(self):
        data = [v for v in self.data.replace('\n', '').replace('\r', '').split(' ') if v]
        size = self.dimension
        distances = []
        for city_from in range(size):
            distances.append([])
            for city_to in range(size):
                distances[-1].append(int(float((data[(city_from * size) + city_to]))))
        return distances

    def _node_coord_parse(self):
        size = self.dimension
        data = (line.strip() for line in self.data.split('\n') if line.strip())
        coords_raw = ([val.strip() for val in line.split(' ') if val.strip()] for line in data)
        coords = list( (float(x), float(y)) for num, x, y in coords_raw )
        
        distances = []
        for city_from in range(size):
            distances.append([None] * size)
            for city_to in range(size):
                distance = self._calculate_distance(coords[city_from], coords[city_to])
                distances[city_from][city_to] = distance
                
        return distances
        
        """
        data = [line.strip() for line in self.data.split('\n') if line]
        size = self.dimension
        
        coords = []
        for line in data:
            num, x, y = [val.strip() for val in line.split(' ') if val.strip()]
            coords.append((float(x), float(y)))
        
        distances = []
        
        for city_from in range(size):
            distances.append([])
            for city_to in range(size):
                distances[-1].append(self._calculate_distance(coords[city_from], coords[city_to]))
        return distances
        """

    def _get_distance_vector(self):
        data = [line.strip() for line in self.data.split('\n') if line]
        size = self.dimension
        coords = []
        for line in data:
            num, x, y = [val.strip() for val in line.split(' ') if val.strip()]
            coords.append((float(x), float(y)))
        return coords
    
    def geo_coord(self, val):
        deg = nint(val)
        m = val - deg
        return math.pi * (deg + 5.0 * m / 3.0) / 180.0
        
    def _calculate_distance(self, city_from, city_to):
        x1, y1 = city_from
        x2, y2 = city_to
        xd = (x1 - x2) ** 2
        yd = (y1 - y2) ** 2
        if self.edge_weight_type == 'EUC_2D':
            dist = sqrt(xd + yd)
        
        elif self.edge_weight_type == 'ATT':
            dist = sqrt((xd + yd) / 10.0)
            if nint(dist) < dist:
                dist = nint(dist) + 1
            else:
                dist = nint(dist)
                
        elif self.edge_weight_type == 'GEO':
            lat1 = self.geo_coord(x1)
            long1 = self.geo_coord(y1)
            lat2 = self.geo_coord(x2)
            long2 = self.geo_coord(y2)
            RRR = 6378.388
            q1 = math.cos(long1 - long2)
            q2 = math.cos(lat1 - lat2)
            q3 = math.cos(lat1 + lat2)
            return int(RRR * math.acos(0.5 * ((1.0 + q1) * q2 - (1.0 - q1) * q3)) + 1.0)

        else:
            raise ParseException("edge_weight_type not implemented")
        
        return nint(dist)


