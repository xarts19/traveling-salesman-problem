#pragma once

#include "tsp_types.h"

#include <set>
#include <algorithm>
#include <list>
#include <vector>
#include <string>

/*
    Implementation functions for H and Genetic algorithms.

        Option name                               Type   Default Value
            population_size                        int      100
            crossovers                             int        0
            crossovers_per_population_100          int       10
            mutations                              int        0
            mutations_per_population_100           int        1

*/

namespace TSP
{
    template <typename T_Container>
    class Solver;

    bool contains(const IntVector* path, unsigned int start, unsigned int end, int what);
    unsigned int find(const IntVector* path, unsigned int start, unsigned int end, int what, bool& found);

    struct Unit
    {
        IntVector* path;
        long long cost;
    };

    bool compare_units(Unit unit1, Unit unit2);
    double inverse_cost(Unit unit);

    // forward declaration
    template <typename T_DistancesType, typename T_Recombinator>
    class GeneticModel;

    class CycleRecombinator
    {
    public:
        template <typename T_DistancesType, typename T_Recombinator>
        Unit operator()(const GeneticModel<T_DistancesType, typename T_Recombinator>* model, const Unit& parent_1, const Unit& parent_2);
    };

    class OrderRecombinator
    {
    public:
        template <typename T_DistancesType, typename T_Recombinator>
        Unit operator()(const GeneticModel<T_DistancesType, typename T_Recombinator>* model, const Unit& parent_1, const Unit& parent_2);
    };

    // cycle / ordered crossovers
    class MixedRecombinator
    {
    public:
        template <typename T_DistancesType, typename T_Recombinator>
        Unit operator()(const GeneticModel<T_DistancesType, typename T_Recombinator>* model, const Unit& parent_1, const Unit& parent_2);
    };

    class HRecombinator
    {
    public:
        template <typename T_DistancesType, typename T_Recombinator>
        Unit operator()(const GeneticModel<T_DistancesType, typename T_Recombinator>* model, const Unit& parent_1, const Unit& parent_2);
    };

    template <typename T_DistancesType, typename T_Recombinator>
    class GeneticModel {

    public:
        GeneticModel(Solver<T_DistancesType>* solver, const T_DistancesType* distances);
        ~GeneticModel();

        void make_one_step();
        IntVector* get_record();
        long long get_record_cost();

    private:
        unsigned int _number_of_cities;
        std::vector<Unit> population;

        std::string problem_type;
        const T_DistancesType* distances;

        Unit record;

        int population_size;
        int crossover_pairs_number;
        int mutations_number;

        Solver<T_DistancesType>* _solver;

        void find_and_save_record(std::vector<Unit> candidates);

        void create_initial_population(int population_size);
        Unit inverse_mutation(const Unit& mutation_candidate);
        Unit transpose_mutation(const Unit& mutation_candidate);
        void replacement(std::vector<Unit> new_units);
        std::vector<int> roulette(std::vector<Unit> candidates, unsigned int number_to_select);

        friend class CycleRecombinator;
        friend class OrderRecombinator;
        friend class MixedRecombinator;
        friend class HRecombinator;
    };

};
