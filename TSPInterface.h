#pragma once

#ifdef _WIN32

#ifdef TSP_EXPORTS
#define TSP_API __declspec(dllexport)
#else
#define TSP_API __declspec(dllimport)
#endif /* TSP_EXPORTS */

#else /* _WIN32 */

#define TSP_API

#endif /* _WIN32 */

#include "tsp_types.h"
#include "common.h"

extern "C"
{
    /* Solver management */
    TSP_API void* new_solver_matrix( unsigned int number_of_cities, const IntMatrix* distance_matrix, Algorithms alg, int memory_limit_mb );
    TSP_API void* new_solver_list( unsigned int number_of_cities, const IntMatrList* coord_list, Algorithms alg, int memory_limit_mb );
    TSP_API void  delete_solver( void* solver );

    /* Solver options */
    TSP_API void enable_logging( void* solver, LogLevel level );
    TSP_API void disable_logging( void* solver );
    TSP_API void set_seed( void* solver, int seed );

    TSP_API void set_edge_type_symmetric( void* solver );
    TSP_API void set_edge_type_asymmetric( void* solver );
    TSP_API void set_int_option( void* solver, const char* name, int option );
    TSP_API void set_double_option( void* solver, const char* name, double option );
    TSP_API void set_bool_option( void* solver, const char* name, int option );
    TSP_API void clear_options( void* solver );

    TSP_API void set_initial_approximation( void* solver, const IntVector* path );
    TSP_API void disable_init_approx( void* solver );

    /* Solver algorithms */
    TSP_API int run_algorithm( void* solver, IntVector* result, long long& cost );
}
