#include "TSPGenetic.h"

#include "TSPSolver.h"
#include "TSPNeighborhood.h"

using namespace TSP;

bool TSP::contains(const IntVector* path, unsigned int start, unsigned int end, int what)
{
    assert( start < end && end <= path->size );
    for (unsigned int i = start; i < end; ++i)
        if (path->at(i) == what)
            return true;
    return false;
}

unsigned int TSP::find(const IntVector* path, unsigned int start, unsigned int end, int what, bool& found)
{
    assert( start < end && end <= path->size );
    for (unsigned int i = start; i < end; ++i)
        if (path->at(i) == what)
        {
            found = true;
            return i;
        }
    found = false;
    return static_cast<unsigned int>(-1);
}

bool TSP::compare_units( Unit unit1, Unit unit2 )
{
    return ( unit1.cost < unit2.cost );
}

double TSP::inverse_cost( Unit unit )
{
    return 1.0 / unit.cost;
}

template <typename T_DistancesType, typename T_Recombinator>
Unit TSP::CycleRecombinator::operator()(const GeneticModel<T_DistancesType, typename T_Recombinator>* model,
                                        const Unit& parent_1, const Unit& parent_2)
{
    Unit child;
    child.path = IntVector::alloc(model->_number_of_cities);
    child.path->fill(-1);

    int parent_num = rand() % 2;

    for ( unsigned int i = 0; i < model->_number_of_cities; ++i )
    {
        // if this node is not assined yet
        if ( child.path->at(i) == -1 )
        {
            const IntVector* path_1 = parent_1.path;
            const IntVector* path_2 = parent_2.path;

            // select parents in turns
            if ( parent_num % 2 != 0 )
            {
                path_1 = parent_2.path;
                path_2 = parent_1.path;
            }

            // copy cycle from first random parent into child
            int start_city = path_1->at(i);
            *( child.path->pointer_at(i) ) = start_city;
            int current_city = path_2->at(i);
            int current_city_pos = i;

            while ( current_city != start_city )
            {
                // find current city in first parent
                bool ok = false;

                current_city_pos = find(path_1, 0, model->_number_of_cities, current_city, ok);
                assert(ok);

                assert( current_city == path_1->at(current_city_pos) );
                *( child.path->pointer_at(current_city_pos) ) = current_city;
                current_city = path_2->at(current_city_pos);
            }

            ++parent_num;
        }
    }

    child.cost = calc_cost( model->distances, child.path );

    return child;
}

template <typename T_DistancesType, typename T_Recombinator>
Unit TSP::OrderRecombinator::operator()(const GeneticModel<T_DistancesType, typename T_Recombinator>* model,
                                        const Unit& parent_1, const Unit& parent_2)
{
    Unit child;
    child.path = IntVector::alloc(model->_number_of_cities);

    //    AB|CDEF|GHI + hd|aeic|fbg
    //              \/
    //           ai|CDEF|bgh
    //      because path is circular, [aiCDEFbgh] == [CDEFbghai]
    unsigned int point_1 = rand() % ( model->_number_of_cities / 2 );
    unsigned int point_2 = point_1 + ( model->_number_of_cities / 2 ) - 1;

    // copy central segment to child (from parent_1)
    // child += CDEF    // [CDEF]
    slice_copy(parent_1.path, child.path, point_1, 0, point_2 - point_1);
    unsigned int current_child_path_size = point_2 - point_1;

    // insert cities not present in first parent's central segment into child from second parent's last segment
    // child += bg    // [CDEFbg]
    for ( unsigned int i = point_2; i < model->_number_of_cities; i++ )
    {
        if ( !contains(parent_1.path, point_1, point_2, parent_2.path->at(i)) )
            *( child.path->pointer_at(current_child_path_size++) ) = parent_2.path->at(i);
    }

    // insert cities not present in first parent's central segment into child from second parent's beginning
    // child += hai     // [CDEFbghai]
    for ( unsigned int i = 0; i < point_2; i++ )
    {
        if ( !contains(parent_1.path, point_1, point_2, parent_2.path->at(i)) )
            *( child.path->pointer_at(current_child_path_size++) ) = parent_2.path->at(i);
    }

    child.cost = calc_cost( model->distances, child.path );

    return child;
}

template <typename T_DistancesType, typename T_Recombinator>
inline Unit TSP::MixedRecombinator::operator()(const GeneticModel<T_DistancesType, typename T_Recombinator>* model,
                                               const Unit& parent_1, const Unit& parent_2)
{
    // choose random crossover
    if ( rand() % 2 )
    {
        return OrderRecombinator()( model, parent_1, parent_2 );
    }
    else
    {
        return CycleRecombinator()( model, parent_1, parent_2 );
    }
}

template <typename T_DistancesType, typename T_Recombinator>
inline Unit TSP::HRecombinator::operator()(const GeneticModel<T_DistancesType, typename T_Recombinator>* model,
                                           const Unit& parent_1, const Unit& parent_2)
{
    Unit source;
    Unit target;
    if (parent_1.cost > parent_2.cost)
    {
        source = parent_1;
        target = parent_2;
    }
    else
    {
        source = parent_2;
        target = parent_1;
    }

    Unit child;
    child.path = source.path->copy();
    child.cost = source.cost;
    
    unsigned int swap_distance = 0;

    long long min_cost = std::numeric_limits<long long>::max();
    IntVector* min_path = IntVector::alloc(model->_number_of_cities);
    long long current_cost = child.cost;

    int step = 0;
    const int r1 = 3;
    const int r2 = 3;
    const int r3 = 3;

    // generate all paths on the ray from worse parent to better
    for (unsigned int i = 0; i < model->_number_of_cities; ++i)
    {
        if ( child.path->at(i) != target.path->at(i) )
        {
            ++step;
            ++swap_distance;
            unsigned int pos = child.path->find( target.path->at(i), i );

            current_cost += DeltaCost<Asymmetric>::nodes_transposed(model->distances, child.path, i, pos);

            child.path->swap(i, pos);

            if (current_cost < min_cost && step >= r1)
            {
                // check if we already reached parent_2
                bool finished = true;
                int steps_left = 0;
                for (unsigned int j = i + 1; j < model->_number_of_cities; ++j)
                {
                    if (child.path->at(j) != target.path->at(j))
                    {
                        steps_left += 1;
                        if (steps_left >= r2)
                        {
                            finished = false;
                            break;
                        }
                    }
                }

                if (!finished)
                {
                    slice_copy(child.path, min_path, 0, 0, model->_number_of_cities);
                    min_cost = current_cost;
                }
            }
        }
    }

    if (swap_distance < (model->_number_of_cities / 2))
        swap_distance = (model->_number_of_cities / 2);

    step = 0;

    // generate all paths on the ray beyond second parent
    for (unsigned int i = 0, j = 0; i < model->_number_of_cities && j < swap_distance; ++i, ++j)
    {
        if ( child.path->at(i) == source.path->at(i) )
        {
            ++step;
            unsigned int pos = i + 1 + rand() % (model->_number_of_cities - i - 1);

            current_cost += DeltaCost<Asymmetric>::nodes_transposed(model->distances, child.path, i, pos);

            child.path->swap(i, pos);

            if (current_cost < min_cost && step >= r3)
            {
                slice_copy(child.path, min_path, 0, 0, model->_number_of_cities);
                min_cost = current_cost;
            }
        }
    }

    assert(min_cost != std::numeric_limits<long long>::max());

    slice_copy(min_path, child.path, 0, 0, model->_number_of_cities);
    child.cost = min_cost;

    IntVector::dealloc(min_path);

    return child;
}

template <typename T_DistancesType, typename T_Recombinator>
GeneticModel<T_DistancesType, T_Recombinator>::
GeneticModel(Solver<T_DistancesType>* solver, const T_DistancesType* distances) : 
        _number_of_cities(distances->size),
        distances( distances ),
        _solver(solver)
{
    if (solver->options().hasopt<int>("population_size"))
        population_size = solver->options().getopt<int>("population_size");
    else
        population_size = 100;
    
    record.path = IntVector::alloc(_number_of_cities);
    random_path(_number_of_cities,record.path);
    record.cost = calc_cost(distances, record.path);

    create_initial_population( population_size );

    if (solver->options().hasopt<int>("crossovers"))
        crossover_pairs_number = solver->options().getopt<int>("crossovers");
    else
        crossover_pairs_number = 10;

    if (solver->options().hasopt<int>("crossovers_per_population_100"))
        crossover_pairs_number += static_cast<int>(solver->options().getopt<int>("crossovers_per_population_100") * (population_size / 100.0));
    else
        crossover_pairs_number += static_cast<int>(10 * (population_size / 100.0));

    if (solver->options().hasopt<int>("mutations"))
        mutations_number = solver->options().getopt<int>("mutations");
    else
        mutations_number = 10;

    if (solver->options().hasopt<int>("mutations_per_population_100"))
        mutations_number += static_cast<int>(solver->options().getopt<int>("mutations_per_population_100") * (population_size / 100.0));
    else
        mutations_number += static_cast<int>(population_size / 100.0);
}

template <typename T_DistancesType, typename T_Recombinator>
GeneticModel<T_DistancesType, T_Recombinator>::~GeneticModel()
{
    IntVector::dealloc(record.path);
    for (unsigned int i = 0; i < population.size(); ++i)
        IntVector::dealloc(population[i].path);
}

template <typename T_DistancesType, typename T_Recombinator>
void GeneticModel<T_DistancesType, T_Recombinator>::make_one_step()
{
    std::vector<Unit> new_units;

    for ( int i = 0; i < crossover_pairs_number; i++ )
    {
        std::vector<int> parent_indeces = roulette( population, 2 );
        std::vector<Unit> parents;

        for ( unsigned int j = 0; j < parent_indeces.size(); j++ )
        {
            parents.push_back( population[parent_indeces[j]] );
        }

        //sort(parents.begin(), parents.end(), compare_units);
        Unit child;

        // choose random crossover
        child = T_Recombinator()( this, parents[0], parents[1] );

        _solver->set_initial_approximation(child.path);
        int res = _solver->local_search( child.path, child.cost );
        UNUSED(res);
        assert(res == 0);

        new_units.push_back( child );
    }

    for ( int i = 0; i < mutations_number; i++ )
    {
        // int mutation_candidate_index = rand() % population.size();
        //Unit mutated = transpose_mutation(population[mutation_candidate_index]);
        Unit mutant = transpose_mutation( population[roulette( population, 1 )[0]] );

        _solver->set_initial_approximation(mutant.path);
        int res = _solver->local_search( mutant.path, mutant.cost );
        UNUSED(res);
        assert(res == 0);

        new_units.push_back( mutant );
    }

    replacement( new_units );
}

template <typename T_DistancesType, typename T_Recombinator>
IntVector* GeneticModel<T_DistancesType, T_Recombinator>::get_record()
{
    return record.path;
}

template <typename T_DistancesType, typename T_Recombinator>
long long GeneticModel<T_DistancesType, T_Recombinator>::get_record_cost()
{
    return record.cost;
}

template <typename T_DistancesType, typename T_Recombinator>
void
GeneticModel<T_DistancesType, T_Recombinator>::
find_and_save_record( std::vector<Unit> candidates )
{
    int record_index = -1;

    for ( unsigned int i = 1; i < candidates.size(); i++ )
    {
        if ( candidates[i].cost < record.cost )
        {
            record.cost = candidates[i].cost;
            record_index = i;
        }
    }

    if (record_index != -1)
    {
        slice_copy(candidates[record_index].path, record.path, 0, 0, _number_of_cities);
    }
}

template <typename T_DistancesType, typename T_Recombinator>
void
GeneticModel<T_DistancesType, T_Recombinator>::
create_initial_population( int population_size )
{
    for ( int i = 0; i < population_size; i++ )
    {
        Unit unit;
        unit.path = IntVector::alloc(_number_of_cities);

        random_path(_number_of_cities, unit.path);

        _solver->set_initial_approximation(unit.path);
        int res = _solver->local_search( unit.path, unit.cost );
        UNUSED(res);
        assert(res == 0);
        population.push_back( unit );
    }

    find_and_save_record( population );
}

template <typename T_DistancesType, typename T_Recombinator>
Unit
GeneticModel<T_DistancesType, T_Recombinator>::
inverse_mutation( const Unit& mutation_candidate )
{
    Unit mutant;
    mutant.path = IntVector::alloc(_number_of_cities);

    // select mutation segment
    int point_1 = rand() % ( _number_of_cities - 1 );
    int point_2 = point_1 + 1 + rand() % ( _number_of_cities - point_1 - 1 );

    if (_solver->is_symmetric())
        mutant.cost = mutation_candidate.cost + DeltaCost<Symmetric>::segment_inversed( distances, mutation_candidate.path, point_1, point_2 );
    else
        mutant.cost = mutation_candidate.cost + DeltaCost<Asymmetric>::segment_inversed( distances, mutation_candidate.path, point_1, point_2 );

    slice_copy(mutation_candidate.path, mutant.path, 0, 0, point_1);
    slice_copy_reversed(mutation_candidate.path, mutant.path, point_1, point_1, point_2 - point_1);
    slice_copy(mutation_candidate.path, mutant.path, point_1 + point_2, point_1 + point_2, _number_of_cities - point_2);
    
    return mutant;
}

template <typename T_DistancesType, typename T_Recombinator>
Unit
GeneticModel<T_DistancesType, T_Recombinator>::
transpose_mutation( const Unit& mutation_candidate )
{
    Unit mutant;
    mutant.path = mutation_candidate.path->copy();
    mutant.cost = mutation_candidate.cost;

    // select number of transpositions: 1..3
    int k = 1 + rand() % ( _number_of_cities / 6 );

    for ( int i = 0; i < k; i++ )
    {
        int point_1 = rand()               % ( _number_of_cities - 1 );
        int point_2 = point_1 + 1 + rand() % ( _number_of_cities - 1 - point_1 );
        // transpose segment
        // a b c d
        // a d c b
        mutant.cost = mutant.cost + DeltaCost<Asymmetric>::nodes_transposed( distances, mutant.path, point_1, point_2 );

        mutant.path->swap(point_1, point_2);
    }

    return mutant;
}

template <typename T_DistancesType, typename T_Recombinator>
void
GeneticModel<T_DistancesType, T_Recombinator>::
replacement( std::vector<Unit> new_units )
{
    find_and_save_record( new_units );
    unsigned int before = population.size();

    // append children
    population.insert( population.end(), new_units.begin(), new_units.end() );

    // sort in ascending order by cost function
    sort( population.begin(), population.end(), compare_units );


    while ( population.size() > before )
    {
        IntVector::dealloc( population[population.size() - 1].path );
        population.pop_back();
    }
}

template <typename T_DistancesType, typename T_Recombinator>
std::vector<int>
GeneticModel<T_DistancesType, T_Recombinator>::
roulette( std::vector<Unit> candidates, unsigned int number_to_select )
{
    // stochastic universal sampling
    assert( 0 < number_to_select && number_to_select <= candidates.size() );

    std::vector<int> chosen;

    double cost_sum = 0;

    for ( unsigned int i = 0; i < candidates.size(); i++ )
    {
        cost_sum += inverse_cost( candidates[i] );
    }

    double random = ( rand() % 10000 ) / ( 10000.0 * number_to_select );
    //print(random);

    std::vector<double> cumulative_probability;
    cumulative_probability.push_back( inverse_cost( candidates[0] ) / cost_sum );

    for ( unsigned int i = 1; i < candidates.size(); i++ )
    {
        cumulative_probability.push_back( cumulative_probability[i - 1] + ( inverse_cost( candidates[i] ) / cost_sum ) );
    }

    //print_vect(cumulative_probability);

    for ( unsigned int i = 0; i < cumulative_probability.size(); i++ )
    {
        if ( random <= cumulative_probability[i] )
        {
            random += 1.0 / number_to_select;
            chosen.push_back( i );
            //print(int(i));
        }

        if ( chosen.size() == number_to_select ) break;
    }

    return chosen;
}



template class TSP::GeneticModel<IntMatrix, MixedRecombinator>;
template class TSP::GeneticModel<IntMatrList, MixedRecombinator>;

template class TSP::GeneticModel<IntMatrix, HRecombinator>;
template class TSP::GeneticModel<IntMatrList, HRecombinator>;

