#pragma once

enum Algorithms
{
    LocalSearch = 0,
    SimulatedAnnealing = 1,
    VaryingNeighborhoodSearch = 2,
    IteratedLocalSearch = 3,
    GuidedLocalSearch = 4,
    GeneticAlgorithm = 5,
    AntColonyOptimization = 6,
    GSAlgorithm = 7,
    HAlgorithm = 8
};

enum LogLevel
{
    Debug = 0,
    Info = 1,
    Warning = 2,
    Error = 3,
    Critical = 4
};

namespace TSP
{
    enum EdgeType
    {
        Symmetric,      /* Distance A->B == Distance B->A */
        Asymmetric      /* Distance A->B != Distance B->A */
    };

    enum ContainerType
    {
        FullMatrix,
        AdjList
    };
};