#!/usr/bin/env python

import sys
from optparse import OptionParser
from openpyxl import Workbook
from openpyxl.charts import LineChart, Reference, Series

def _get_type(f):
    if f.endswith("atsp"):
        return "ATSP"
    else:
        return "TSP"

class File():
    def __init__(self, name, size, type, exact):
        self.name = name
        self.size = size
        self.type = type
        self.exact = exact
        self.results = {}

class Result():
    def __init__(self, method, result, time, q):
        self.method = method
        self.result = result
        self.time = time
        self.q = q



def parse(lines, files = None, methods = None):
    if files == None:
        files = {}
    
    if methods == None:
        methods = set()

    method = "Unknown"
    
    for line in lines:
        
        if line.startswith("="):
            method = line.strip()[1:]
            methods.add(method)
        else:
            rows = [v.strip() for v in line.split("|")]
            file, size, sol, exact, q, time = 0, 0, 0, 0, 0, 0
            if (rows[1] == "TSP" or rows[1] == "ATSP"):
                file, _, size, _, sol, exact, q, time = rows
            else:
                file, size, _, sol, exact, q, time = rows
                
            r = Result(method, int(sol), float(time), float(q))
            f = files.get(file)
            if f is None:
                f = File(file, int(size), _get_type(file), int(exact))
                files[file] = f
                
            f.results[method] = r
    
    return files, methods


def write_res_txt(problems, methods, name):
    tsps, atsps = _split_and_sort(problems)
    if len(tsps) > 0:
        _write_res_txt_work(tsps, methods, name + "_tsp.txt")
    if len(atsps) > 0:
        _write_res_txt_work(atsps, methods, name + "_atsp.txt")


def write_res_xls(problems, methods, name):
    tsps, atsps = _split_and_sort(problems)
    
    wb = Workbook()
    
    ws = wb.active
    
    if len(tsps) > 0:
        ws.title = "TSP"
        _write_res_xls_work(ws, tsps, methods)
        if len(atsps) > 0:
            ws = wb.create_sheet()
    
    if len(atsps) > 0:
        ws.title = "ATSP"
        _write_res_xls_work(ws, atsps, methods)
    
    wb.save(filename = name + ".xlsx")
    

def main():
    parser = OptionParser(description = 'Results parser and converter for Travelling Salesman Problem Solver. Converts results to format, suitable for importing from Excel', prog = 'RPtsps', version = "%prog 0.01")
    
    parser.add_option('-f', '--file', dest='files', action="append", help='input file name, can be multiple files')
    parser.add_option('-g', '--generate_xls', dest='generate_xls', action="store_true", help='generate MS Excel file')
    
    opts = parser.parse_args()[0]
    if opts.files == None or len(opts.files) == 0:
        print("No input files specified\n")
        parser.print_help()
        exit(-1)
    
    output_name = ""
    lines = []
    if opts.files[0] == "-":
        lines = sys.stdin.readlines()
        output_name += "stdin"
        f, m = parse(lines)
    else:
        f = {}
        m = set()
        for name in opts.files:
            lines = open(name).readlines()
            output_name += name + "_"
            f, m = parse(lines, f, m)
    
    write_res_txt(f, m, output_name)
    if opts.generate_xls:
        write_res_xls(f, m, output_name)


def _write_res_txt_work(problems, methods, filename):
    with open(filename, "w") as f:
        f.write("Name,Size,Type,Exact")
        for m in methods:
            f.write(",")
            f.write(m)
            f.write(",Result,Time,q%")
        
        f.write("\n")
        for x in problems:
            line = [x.name, str(x.size), x.type, str(x.exact)]
            
            for m in methods:
                line.append("      ")
                res = x.results.get(m)
                if res and int(res.result) > 0:
                    line.extend([str(res.result), "%.3f" % res.time, "%.2f" % res.q])
                else:
                    line.extend(["-", "-", "-"])

            f.write(",".join(line))
            f.write("\n")


def _write_res_xls_work(worksheet, problems, methods):

    method_start = 4
    method_error_offset = 3
    method_next_offset = 4

    header = ["Name", "Size", "Type", "Exact solution"]
    for m in methods:
        header.extend([m, "Result", "Time", "q%"])
    
    start_col_idx = 1
    start_row_idx = 1
    
    col_idx = start_col_idx
    row_idx = start_row_idx
    for item in header:
        worksheet.cell(row = row_idx, column = col_idx).value = item
        col_idx += 1

    table = []
    
    for problem in problems:
        line = [problem.name, problem.size, problem.type, problem.exact]
        
        for m in methods:
            line.append("")
            res = problem.results.get(m)
            if res and int(res.result) > 0:
                line.extend([res.result, res.time, res.q])
            else:
                line.extend(["-", "-", "-"])

        table.append(line)
    
    row_idx += 1
    for line in table:
        col_idx = start_col_idx
        for item in line:
            worksheet.cell(row = row_idx, column = col_idx).value = item
            col_idx += 1
        row_idx += 1
    
    chart = LineChart()
    col_idx = start_col_idx + method_start
    for m in methods:
        first_value = (start_row_idx + 1, col_idx + method_error_offset)
        last_value = (start_row_idx + len(problems), col_idx + method_error_offset)
        values = Reference(worksheet, first_value, last_value)
        series = Series(values, title=m)
        chart.append(series)
        col_idx += method_next_offset
    
    worksheet.add_chart(chart)


def _split_and_sort(problems):
    tsps = []
    atsps = []
    for k, v in problems.items():
        if v.type == "ATSP":
            atsps.append(v)
        else:
            tsps.append(v)
            
    tsps = sorted(tsps, key = lambda x: int(x.size))
    atsps = sorted(atsps, key = lambda x: int(x.size))
    
    return tsps, atsps


if __name__=="__main__":
    main()