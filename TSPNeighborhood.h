#pragma once

#include "tsp_types.h"
#include "common.h"

namespace TSP
{

    namespace Permutations
    {
        inline void swap_2opt( IntVector* path, unsigned int node_1, unsigned int node_2 )
        {
            /*
                Take 2 crossing paths and reorder them to not cross:

                - O   O -             - O - O -
                    X         ==>     
                - O   O -             - O - O -

                ABC-abc-123 => ABC-cba-123

                [0, node_1)[node_1, node_2)[node_2, size) =>
                [0, node_1)[   reversed   )[node_2, size)

            */
            assert( node_1 < node_2 && node_2 < path->size );

            path->reverse_subarray( node_1, node_2 );
        }

        inline void swap_3opt( IntVector* path, unsigned int node_1, unsigned int node_2 )
        {
            /*
                  O---O                 O   O
               1 /     \ 2           1 / \ / \ 2  
                O       O      ==>    O---X---O
                 \     /                 / \  
                  O---O                 O---O
                    3                     3
                ABC-abc-123 => ABC-cba-123

                | [0]      | [node_1]                  | [node_2]        | [size]
                v          v                           v                 v
                [    1    )[             2            )[        3       ) =>
                [    1    )[        3       )[             2            )
                                             ^                            
                                             | [node_1 + (size - node_2)]
            */
            assert(node_1 < node_2 && node_2 < path->size);

            int size = path->size;
            int len_slice_2 = node_2 - node_1;
            int len_slice_3 = size - node_2;

            // choose smaller chunk to store in tmp
            if (node_2 - node_1 <= size - node_2)       // left slice is smaller
            {
                IntVector* tmp = path->copy_segment(node_1, node_2);
                path->move_subarray(node_2, node_1, len_slice_3);
                slice_copy(tmp, path, 0, node_1 + len_slice_3, tmp->size);
                IntVector::dealloc(tmp);
            }
            else
            {
                IntVector* tmp = path->copy_segment(node_2, size);
                path->move_subarray(node_1, node_1 + len_slice_3, len_slice_2);
                slice_copy(tmp, path, 0, node_1, tmp->size);
                IntVector::dealloc(tmp);
            }
        }

        inline void double_bridge( IntVector* path, unsigned int node_1, unsigned int node_2, unsigned int node_3 )
        {
            /*
                        O-O              O-O
                       /   \              X  
                      O     O          O / \ O
                      |     |    ==>   |X   X|
                      O     O          O \ / O
                       \   /              X  
                        O-O              O-O

                AB-abc-123-ijk-CD => AB-ijk-123-abc-CD

                | [0]      | [node_1]                  | [node_2]          | [node_3]         | [size]
                v          v                           v                   v                  v
                [    1    )[             2            )[        3         )[         4       ) =>
                [    1    )[         4       )[        3         )[             2            )
                                              ^                   ^                            
                                              |                   | [node_1 + (size - node_3) + (node_3 - node_2)]
                                              | [node_1 + (size - node_3)]
            */
            assert(node_1 < node_2 && node_2 < node_3 && node_3 < path->size);

            int size = path->size;
            //int len_slice_2 = node_2 - node_1;
            int len_slice_3 = node_3 - node_2;
            int len_slice_4 = size - node_3;

            IntVector* tmp_slice_2 = path->copy_segment(node_1, node_2);
            IntVector* tmp_slice_3 = path->copy_segment(node_2, node_3);

            path->move_subarray(node_3, node_1, len_slice_4);

            slice_copy(tmp_slice_2, path, 0, node_1 + len_slice_4 + len_slice_3, tmp_slice_2->size);
            slice_copy(tmp_slice_3, path, 0, node_1 + len_slice_4              , tmp_slice_3->size);

            IntVector::dealloc(tmp_slice_2);
            IntVector::dealloc(tmp_slice_3);
        }

        inline void reorder_5opt( IntVector* path, unsigned int node_1, unsigned int node_2, unsigned int node_3, unsigned int node_4 )
        {
             /*
                mix subarrays:
                AB-abc-123-ijk-789-CD => AB-789-123-abc-ijk-CD

                | [0]      | [node_1]                  | [node_2]          | [node_3]         | [node_4]       | [size]
                v          v                           v                   v                  v                v
                [    1    )[             2            )[        3         )[         4       )[       5       ) =>
                [    1    )[       5       )[        3         )[             2            )[         4       )
                                            ^                   ^                           ^
                                            |                   |                           | [node_1 + (size - node_4) + (node_3 - node_2) + (node_2 - node_1)]
                                            |                   | [node_1 + (size - node_4) + (node_3 - node_2)]
                                            | [node_1 + (size - node_4)]
            */
            assert(node_1 < node_2 && node_2 < node_3 && node_3 < node_4 && node_4 < path->size);

            int size = path->size;
            int len_slice_2 = node_2 - node_1;
            int len_slice_3 = node_3 - node_2;
            //int len_slice_4 = node_4 - node_3;
            int len_slice_5 = size - node_4;

            IntVector* tmp_slice_2 = path->copy_segment(node_1, node_2);
            IntVector* tmp_slice_3 = path->copy_segment(node_2, node_3);
            IntVector* tmp_slice_4 = path->copy_segment(node_3, node_4);

            path->move_subarray(node_4, node_1, len_slice_5);

            slice_copy(tmp_slice_2, path, 0, node_1 + len_slice_5 + len_slice_3              , tmp_slice_2->size);
            slice_copy(tmp_slice_3, path, 0, node_1 + len_slice_5                            , tmp_slice_3->size);
            slice_copy(tmp_slice_4, path, 0, node_1 + len_slice_5 + len_slice_3 + len_slice_2, tmp_slice_4->size);

            IntVector::dealloc(tmp_slice_2);
            IntVector::dealloc(tmp_slice_3);
            IntVector::dealloc(tmp_slice_4);
        }

    };

    template <EdgeType T_edgetype>
    class DeltaCost
    {
    public:
        template <typename T_Container>
        inline static int segment_inversed(const T_Container* distances, const IntVector* path, unsigned int node_1, unsigned int node_2);

        template <typename T_Container>
        inline static int nodes_transposed(const T_Container* distances, const IntVector* path, unsigned int node_1, unsigned int node_2);
    };

    template <EdgeType T_edgetype>
    template <typename T_Container>
    int DeltaCost<T_edgetype>::nodes_transposed(const T_Container* distances, const IntVector* path, unsigned int node_1, unsigned int node_2)
    {
        assert(distances->size == path->size);
        assert(node_1 < node_2);
        assert(node_2 < path->size);
        /*
            Calculate delta of cost function. Negative value means improvement.
        */
        int start = node_1 - 1;
        int end   = node_2 + 1;

        if ( node_1 == 0 )
            start = path->size - 1;

        if ( node_2 == path->size - 1 )
            end = 0;

        int added_edges_cost =
            distances->at( path->at(start     ), path->at(node_2    ) ) +
            distances->at( path->at(node_2    ), path->at(node_1 + 1) ) +
            distances->at( path->at(node_2 - 1), path->at(node_1    ) ) +
            distances->at( path->at(node_1    ), path->at(end       ) );

        int removed_deges_cost =
            distances->at( path->at(start     ), path->at(node_1    ) ) +
            distances->at( path->at(node_1    ), path->at(node_1 + 1) ) +
            distances->at( path->at(node_2 - 1), path->at(node_2    ) ) +
            distances->at( path->at(node_2    ), path->at(end       ) );

        return added_edges_cost - removed_deges_cost;
    }

    template <EdgeType T_edgetype>
    template <typename T_Container>
    int DeltaCost<T_edgetype>::segment_inversed(const T_Container* distances, const IntVector* path, unsigned int node_1, unsigned int node_2)
    {
        assert(0);
    }

    template <>
    template <typename T_Container>
    int DeltaCost<Symmetric>::segment_inversed(const T_Container* distances, const IntVector* path, unsigned int node_1, unsigned int node_2)
    {
        assert(distances->size == path->size && node_1 < node_2 && node_2 < path->size);
        /*
            Calculate delta of cost function. Negative value means improvement.
            In TSP we only need to change 2 edges.
        */
        int start = node_1 - 1;
        int end   = node_2 + 1;

        if ( node_1 == 0 )
            start = path->size - 1;

        if ( node_2 == path->size - 1 )
            end = 0;

        int total = 0;

        int at_start = path->at(start );
        int at_node_1 = path->at(node_1);
        int at_node_2 = path->at(node_2);
        int at_end = path->at(end);

        total += distances->at(at_start, at_node_2);
        total += distances->at(at_node_1, at_end);
        total -= distances->at(at_start, at_node_1);
        total -= distances->at(at_node_2, at_end);

        return total;

       /* 
        return distances->at( path->at(start ), path->at(node_2    ) ) +
               distances->at( path->at(node_1), path->at(end       ) ) -
               distances->at( path->at(start ), path->at(node_1    ) ) -
               distances->at( path->at(node_2), path->at(end       ) ) ;
               */
    }

    template <>
    template <typename T_Container>
    int DeltaCost<Asymmetric>::segment_inversed(const T_Container* distances, const IntVector* path, unsigned int node_1, unsigned int node_2)
    {
        assert(distances->size == path->size && node_1 < node_2 && node_2 < path->size);
        /*
            Calculate delta of cost function. Negative value means improvement.
            For ATSP we have to recalculate whole cost of reversed segment.
        */
        int delta = 0;

        int start = node_1 - 1;
        int end   = node_2 + 1;

        if ( node_1 == 0 )
            start = path->size - 1;

        if ( node_2 == path->size - 1 )
            end = 0;

        int at_start = path->at(start);
        int at_node_1 = path->at(node_1);
        int at_node_2 = path->at(node_2);
        int at_end = path->at(end);

        // handle first edge
        delta += distances->at( at_start, at_node_2 ) -
                 distances->at( at_start, at_node_1 ) ;

        // rest w/o last
        for ( unsigned int step = 0; step <= node_2 - node_1 - 1; step++ )
        {
            delta += distances->at( path->at(node_2 - step) , path->at(node_2 - step - 1) ) -
                     distances->at( path->at(node_1 + step) , path->at(node_1 + step + 1) ) ;
        }

        // last
        delta += distances->at( at_node_1 , at_end ) -
                 distances->at( at_node_2 , at_end ) ;

        return delta;
    }

    /*
        Base class for cyclic neighborhoods.

        It has some internal pointers that point somewhere into the path that move cyclically with advance() calls.
        It also keeps track of currently active path that can be obtained by current() call.
        Based on that pointers and active path, it can generate new path in the neighborhood with generate() call.
        step() call both advances the pointers and generates new path.

    */
    template <typename T_SpecificNbh>
    class NbhBase
    {
    public:
        NbhBase( int size ) :
            stop_counter(0),
            max_count(-1),
            size(size),
            active_path(NULL)
        {
            save_stop_position();
            active_path = NULL;
        }
        
        // set current position as the start of the cycle
        inline void save_stop_position() { stop_counter = 0; }

        // whether full cycle has been traversed
        inline bool is_finished() const { assert(max_count > 0); return stop_counter >= max_count; }
   
        //inline void randomize() { static_cast<T_SpecificNbh*>(this)->randomize(); }

        // advance counter and generate new path
        inline void step() { advance(); generate(); }

        // get current path
        inline const IntVector* current_path() const { return active_path; }

        // set current path
        inline void set_current_path( IntVector* path ) { active_path = path; }


        // NEED IMPLEMENTING IN CHILDREN

        // advance counter without generating new path
        inline void advance() { static_cast<T_SpecificNbh*>(this)->step(); }

        // generate new path from current path for current pointers' values
        inline void generate() { static_cast<T_SpecificNbh*>(this)->generate(); }

        // calculate delta cost between current path and new path that could be generated now with generate()
        template <typename T_Container>
        inline int get_delta_cost(const T_Container* distances) const { return static_cast<T_SpecificNbh*>(this)->get_delta_cost(distances); }

    private:
        NbhBase& operator=(const NbhBase& rhs);

    protected:
        long long stop_counter;
        long long max_count;      // set by children
        const int size;
        IntVector* active_path;
    };

    /*
        Base class for 2-opt neighborhoods.
    */
    template <typename T_SpecificNbh>
    class Nbh2optBase : public NbhBase< Nbh2optBase<T_SpecificNbh> >
    {
    public:
        Nbh2optBase( int size ) : 
            NbhBase<Nbh2optBase>(size),
            node_1(0),
            node_2(1)
        {
            this->max_count = size - 1;
            this->max_count *= size - 2;
            this->max_count /= 2;
        }

        /*
        void randomize()
        {
            node_1 = rand() % ( size - 2 );
            node_2 = node_1 + ( rand() % ( size - 1 - node_1 ) );
        }
        */

        inline void advance()
        {
            node_2++;

            if ( node_2 >= this->size - 1 )
            {
                node_1++;

                if ( node_1 >= this->size - 2 )
                {
                    node_1 = 0;
                }

                node_2 = node_1 + 1;
            }

            this->stop_counter++;
        }

        inline void generate() { Permutations::swap_2opt( this->active_path, node_1, node_2 + 1 ); }

        template <typename T_Container>
        inline int get_delta_cost(const T_Container* distances) const { return static_cast<T_SpecificNbh*>(this)->get_delta_cost(distances); }

    protected:
        int node_1;     // [0, n-3]
        int node_2;     // [1, n-2]
    };
    

    class Nbh2optSym : public Nbh2optBase<Nbh2optSym>
    {
        /* 
            2-opt neighborhood generator for symmetric TSP ( distances A->B == B->A )
            AB-123-CD -> AB-321-CD
        */
    public:
        Nbh2optSym( int size ) : Nbh2optBase<Nbh2optSym>( size ) {}

        template <typename T_Container>
        inline int get_delta_cost(const T_Container* distances) const
        {
            return DeltaCost<Symmetric>::segment_inversed(distances, current_path(), node_1, node_2);
        }
    };


    class Nbh2optAsym : public Nbh2optBase<Nbh2optAsym>
    {
        /* 
            2-opt neighborhood generator for asymmetric TSP ( distances A->B != B->A )
            AB-123-CD -> AB-321-CD
        */
    public:
        Nbh2optAsym( int size ) : Nbh2optBase<Nbh2optAsym>( size ) {}

        template <typename T_Container>
        inline int get_delta_cost(const T_Container* distances) const
        {
            return DeltaCost<Asymmetric>::segment_inversed(distances, current_path(), node_1, node_2);
        }
    };


    class Nbh3opt : public NbhBase<Nbh3opt>
    {
        /* 
            3-opt neighborhood generator for symmetric TSP ( distances A->B == B->A )

            AB-123-456-CD -> AB-456-123-CD
        */
    public:
        Nbh3opt( int size ) :
            NbhBase<Nbh3opt>( size ),
            node_1(1),
            node_2(2)
        {
            this->max_count = size - 1;
            this->max_count *= size - 2;
            this->max_count /= 2;
        }

        //void randomize();

        inline void advance()
        {
            node_2++;

            if ( node_2 >= size )
            {
                node_1++;

                if ( node_1 >= size - 1 )
                {
                    node_1 = 1;
                }

                node_2 = node_1 + 1;
            }

            stop_counter++;
        }

        inline void generate() { Permutations::swap_3opt(active_path, node_1, node_2); }

        template <typename T_Container>
        inline int get_delta_cost(const T_Container* distances) const
        {
            //Calculate delta of cost path. Negative value means improvement.
            int added_edges_cost =
                distances->at( current_path()->at(node_1 - 1), current_path()->at(node_2) ) +
                distances->at( current_path()->at(size - 1  ), current_path()->at(node_1) ) +
                distances->at( current_path()->at(node_2 - 1), current_path()->at(0     ) );

            int removed_deges_cost =
                distances->at( current_path()->at(node_1 - 1), current_path()->at(node_1) ) +
                distances->at( current_path()->at(node_2 - 1), current_path()->at(node_2) ) +
                distances->at( current_path()->at(size - 1  ), current_path()->at(0     ) );

            return added_edges_cost - removed_deges_cost;
        }

    private:
        int node_1;     // [0, n-2]
        int node_2;     // [1, n-1]
    };


    class Nbh4opt : public NbhBase<Nbh4opt>
    {
        /* 
            4-opt neighborhood generator for symmetric TSP ( distances A->B == B->A )

            AB-123-456-CD -> AB-456-123-CD
        */
    public:
        Nbh4opt( int size ) :
            NbhBase<Nbh4opt>( size ),
            node_1(1),
            node_2(2),
            node_3(3)
        {
            node_increment = (size < 1000 ? 1 : size / 1000);

            this->max_count = (size - 1) / node_increment;
            this->max_count *= (size - 2) / node_increment;
            this->max_count *= (size - 3) / node_increment;
            this->max_count /= 6;
        }

        //void randomize();

        inline void advance()
        {
            node_3 += node_increment;
            if ( node_3 >= size )
            {
                node_2 += node_increment;
                if ( node_2 >= size - 1 )
                {
                    node_1 += node_increment;
                    if ( node_1 >= size - 2 )
                        node_1 = 1 + rand() % node_increment;

                    node_2 = node_1 + 1;
                }
                node_3 = node_2 + 1;
            }
            stop_counter++;
        }

        inline void generate() { Permutations::double_bridge(active_path, node_1, node_2, node_3); }

        template <typename T_Container>
        inline int get_delta_cost(const T_Container* distances) const
        {
            //Calculate delta of cost path. Negative value means improvement.
            int added_edges_cost =
                distances->at( current_path()->at(node_1 - 1), current_path()->at(node_3) ) +
                distances->at( current_path()->at(size - 1  ), current_path()->at(node_2) ) +
                distances->at( current_path()->at(node_3 - 1), current_path()->at(node_1) ) +
                distances->at( current_path()->at(node_2 - 1), current_path()->at(0     ) );

            int removed_deges_cost =
                distances->at( current_path()->at(node_1 - 1), current_path()->at(node_1) ) +
                distances->at( current_path()->at(node_2 - 1), current_path()->at(node_2) ) +
                distances->at( current_path()->at(node_3 - 1), current_path()->at(node_3) ) +
                distances->at( current_path()->at(size - 1  ), current_path()->at(0     ) );

            return added_edges_cost - removed_deges_cost;
        }

    private:
        int node_1;     // [0, n-3]
        int node_2;     // [1, n-2]
        int node_3;     // [2, n-1]

        int node_increment;
    };


    class Nbh5opt : public NbhBase<Nbh5opt>
    {
        /* 
            4-opt neighborhood generator for symmetric TSP ( distances A->B == B->A )

            AB-123-456-CD -> AB-456-123-CD
        */
    public:
        Nbh5opt( int size ) :
            NbhBase<Nbh5opt>( size ),
            node_1(1),
            node_2(2),
            node_3(3),
            node_4(4)
        {
            node_increment = (size < 200 ? 1 : size / 200);

            this->max_count = (size - 1) / node_increment;
            this->max_count *= (size - 2) / node_increment;
            this->max_count *= (size - 3) / node_increment;
            this->max_count *= (size - 4) / node_increment;
            this->max_count /= 20;
        }

        //void randomize();

        inline void advance()
        {
            node_4 += node_increment;
            if ( node_4 >= size )
            {
                node_3 += node_increment;
                if ( node_3 >= size - 1 )
                {
                    node_2 += node_increment;
                    if ( node_2 >= size - 2 )
                    {
                        node_1 += node_increment;
                        if ( node_1 >= size - 3 )
                            node_1 = 1 + rand() % node_increment;

                        node_2 = node_1 + 1;
                    }
                    node_3 = node_2 + 1;
                }
                node_4 = node_3 + 1;
            }
            stop_counter++;
        }

        inline void generate() { Permutations::reorder_5opt(active_path, node_1, node_2, node_3, node_4); }

        template <typename T_Container>
        inline int get_delta_cost(const T_Container* distances) const
        {
            //Calculate delta of cost path. Negative value means improvement.
            int added_edges_cost =
                distances->at( current_path()->at(node_1 - 1), current_path()->at(node_4) ) +
                distances->at( current_path()->at(size - 1  ), current_path()->at(node_2) ) +
                distances->at( current_path()->at(node_3 - 1), current_path()->at(node_1) ) +
                distances->at( current_path()->at(node_2 - 1), current_path()->at(node_3) ) +
                distances->at( current_path()->at(node_4 - 1), current_path()->at(0     ) );

            int removed_deges_cost =
                distances->at( current_path()->at(node_1 - 1), current_path()->at(node_1) ) +
                distances->at( current_path()->at(node_2 - 1), current_path()->at(node_2) ) +
                distances->at( current_path()->at(node_3 - 1), current_path()->at(node_3) ) +
                distances->at( current_path()->at(node_4 - 1), current_path()->at(node_4) ) +
                distances->at( current_path()->at(size - 1  ), current_path()->at(0     ) );

            return added_edges_cost - removed_deges_cost;
        }

    private:
        int node_1;     // [0, n-3]
        int node_2;     // [1, n-2]
        int node_3;     // [2, n-1]
        int node_4;     // [3, n-1]

        int node_increment;
    };

};
