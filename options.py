#!/usr/bin/env python

import logging

Algorithms = \
{
    'LS'    : 0,
    'SA'    : 1,
    'VNS'   : 2,
    'ILS'   : 3,
    'GLS'   : 4,
    'GA'    : 5,
    'ACO'   : 6,
    'GS'    : 7,
    'H'     : 8
}

class LogLevels(object):
    Debug = 0
    Info = 1
    Warning = 2
    Error = 3
    Critical = 4

LogLevelMap = \
{
    LogLevels.Debug     : logging.DEBUG,
    LogLevels.Info      : logging.INFO,
    LogLevels.Warning   : logging.WARNING,
    LogLevels.Error     : logging.ERROR,
    LogLevels.Critical  : logging.CRITICAL
}

LogLevelStrings = \
{
    "Debug"    : LogLevels.Debug,
    "Info"     : LogLevels.Info,
    "Warning"  : LogLevels.Warning,
    "Error"    : LogLevels.Error,
    "Critical" : LogLevels.Critical,
}

class Options(object):

    def __init__(self):
        # defaults:
        self.log_level = LogLevels.Warning
        self.files = []
        self.method = None
        self.initials_extension = None
        self.restarts = 1
        self.multiproc = False
        self.seed = None
        self.progress = False
        self.progress_detailed = False
        self.filter_size = 1000000
        self.memory = 2048
        self.time = 60 * 60 * 24
        self.custom = {}
        
    def __str__(self):
        return "Not implemented"
        