#include "TSPSolver.h"

#define UNUSED( x ) ( &reinterpret_cast< const int& >( x ) )

template <typename T_Container>
IntVector* TSP::Solver<T_Container>::create_initial_path() const
{
    if (_use_init_approx)
    {
        return _initial_approximation->copy();
    }
    else
    {
        IntVector* init_path = IntVector::alloc(_number_of_cities);
        random_path( _number_of_cities, init_path );
        return init_path;
    }
}

template <typename T_Container>
int TSP::Solver<T_Container>::get_time_constraints(unsigned int size) const
{
	if (options().hasopt<int>("time_limit"))
		return options().getopt<int>("time_limit");

    if (_type == Symmetric)
    {
        if (size < 100)
            return 100;
        else if (size < 500)
            return 100;
        else if (size < 1000)
            return 200;
        else if (size < 5000)
            return 500;
        else if (size < 10000)
            return 500;
        else
            return 500;
    }
    else if (_type == Asymmetric)
    {
        if (size < 50)
            return 10;
        else if (size < 100)
            return 30;
        else if (size < 200)
            return 120;
        else if (size < 300)
            return 240;
        else if (size < 400)
            return 480;
        else
            return 600;
    }
    else
        assert(0);
    return 0;
}

template <typename T_Container>
bool TSP::Solver<T_Container>::time_is_out(int time_limit) const
{
	return (_timer.time_elapsed_s() > time_limit);
}

template <typename T_Container>
int TSP::Solver<T_Container>::local_search( IntVector* result, long long& cost, bool silent ) const
{
    // Simplest local search
    // Select appropriate neighborhood class
    switch (_type)
    {
    case Symmetric:
        return local_search_worker<Nbh2optSym>(result, cost, silent);
        break;
    case Asymmetric:
        return local_search_worker<Nbh2optAsym>(result, cost, silent);
        break;
    default:
        assert(0 && "_type != Symmetric or Asymmetric");
        break;
    }
    return 1;
}

template <typename T_Container>
template <typename T_Nbh>
int TSP::Solver<T_Container>::local_search_worker( IntVector* result, long long& cost, bool ) const
{
	int time_limit = get_time_constraints(_number_of_cities);

    IntVector* current_path = create_initial_path();
    cost = calc_cost( _distance_matrix, current_path );

    // init neighborhood
    T_Nbh neighbors(_number_of_cities);
    neighbors.set_current_path(current_path);

    int delta_cost = 0;

    // while not all neighbors were checked
    for (;;)
    {
        if (time_is_out(time_limit))
        {
			logger().info() << "Time ran out:" << _timer.time_elapsed_s() << "s";
            break;
        }

        delta_cost = neighbors.get_delta_cost(_distance_matrix);

        // accept solution (less is better)
        if ( delta_cost < 0 )
        {
            neighbors.generate();
            neighbors.save_stop_position();
            cost += delta_cost;
            assert(cost > 0);
            //VT::Logger::log<VT::Logger::Info>("\r                                                                       \r"
            //                                  "Current: %lld", cost);

        }
        // all neighbors was checked
        else if ( neighbors.is_finished() )
        {
            break;
        }

        neighbors.advance();
    }

    slice_copy( neighbors.current_path(), result, 0, 0, _number_of_cities );
    IntVector::dealloc( current_path );

    return 0;
}

// classic annealing
//      p = min{ 1, exp( - (f(y)-f(x))/ T ) }
//      r = random [0, 1]
//      if (p >= r) => accept solution
class SAPlugins
{
public:
    SAPlugins() :
        init_probability_level(10.0),
        iterations(1000000),
        final_probability_level(0.001)
    {}

    inline double probability(int delta_cost, long long /*cost*/, double probability_level)
    {
        // chance to accept tour based on its cost and temperature
        // cheaper tours and high temperature improve chances
        return exp( -delta_cost / probability_level );
    }

    inline double random(double probability_level)
    {
        UNUSED(probability_level);
        return ( rand() % 10000 ) / 10000.0;
    }

    inline double update_probability_level(double probability_level)
    {
       /* if (probability_level > 0.5)
            return probability_level - 0.004;
        else if (probability_level > 0.2)
            return probability_level - 0.0015;
        else*/
            return probability_level * 0.98;
    }

    inline bool is_finished(double probability_level)
    {
        return probability_level < final_probability_level;
    }

    double init_probability_level;
    int iterations;
    double final_probability_level;
};

// G -alg
//      mu[0] = 0
//      p = max{ 0, min{ 1, 1 - ( (f(y)-f(x)) / phi * |f(x)| ) } }
//      r = m + (1 - m)*random[0, 1]
//      if (p >= r) => accept solution
//      mu[i+1] = G(U) ; U = left golden ratio (1 - 0.6180339) of [mu, 1] ; u = mu + (1 - mu)*0.3819661
//         G(z) = min{ 1, (z^(1/k)+H)^k }; k = {1,2,3} ~ 2 ; H = (0, 1) ~ 0.01 ;
//         e = [0.01, 0.05] (stop condition)
class GSAlgPlugins
{
public:
    GSAlgPlugins() :
        init_probability_level(0.5),
        iterations(10000),
        final_probability_level(1 - 0.0000001)
    {}

    inline double probability(int delta_cost, long long cost, double probability_level)
    {
        UNUSED(probability_level);
        return 1 - delta_cost / ( 0.001/*0.3 phi*/ * double(cost) );
    }

    inline double random(double probability_level)
    {
        return probability_level + (1 - probability_level) * ( rand() % 10000 ) / 10000.0;
    }

    inline double update_probability_level(double probability_level)
    {
        iterations = probability_level > 0.9 ? 10000000 : 100000;
        return probability_level + (1 - probability_level) * 0.13819661;
    }

    inline bool is_finished(double probability_level)
    {
        return probability_level >= final_probability_level;
    }

    double init_probability_level;
    int iterations;
    double final_probability_level;
};

template <typename T_Container>
int TSP::Solver<T_Container>::simulated_annealing( IntVector* result, long long& cost )
{
    // Select appropriate neighborhood class
    switch (_type)
    {
    case Symmetric:
        return probabilistic_modeling<Nbh2optSym, SAPlugins>(result, cost);
        break;
    case Asymmetric:
        return probabilistic_modeling<Nbh2optAsym, SAPlugins>(result, cost);
        break;
    default:
        assert(0 && "_type != Symmetric or Asymmetric");
        break;
    }
    return 1;
}

template <typename T_Container>
int TSP::Solver<T_Container>::gs_algorithm( IntVector* result, long long& cost )
{
    // Select appropriate neighborhood class
    switch (_type)
    {
    case Symmetric:
        return probabilistic_modeling<Nbh2optSym, GSAlgPlugins>(result, cost);
        break;
    case Asymmetric:
        return probabilistic_modeling<Nbh2optAsym, GSAlgPlugins>(result, cost);
        break;
    default:
        assert(0 && "_type != Symmetric or Asymmetric");
        break;
    }
    return 1;
}

template <typename T_Container>
template <typename T_Nbh, typename T_Plugin>
int TSP::Solver<T_Container>::probabilistic_modeling( IntVector* result, long long& cost )
{
	int time_limit = get_time_constraints(_number_of_cities);
    bool progress = options().hasopt<bool>("progress") && options().getopt<bool>("progress");

    IntVector* current_path = create_initial_path();
    cost = calc_cost( _distance_matrix, current_path );

    this->local_search(current_path, cost);

    // init neighborhood
    T_Nbh neighbors(_number_of_cities);
    neighbors.set_current_path(current_path);

    T_Plugin plugin;
    
    if (options().hasopt<double>("init_probability_level"))
        plugin.init_probability_level = options().getopt<double>("init_probability_level");

    if (options().hasopt<int>("iterations"))
        plugin.iterations = options().getopt<int>("iterations");
        
    if (options().hasopt<double>("final_probability_level"))
        plugin.final_probability_level = options().getopt<double>("final_probability_level");

    double probability_level = plugin.init_probability_level;

    // keep track of best solution
    IntVector* record_path = neighbors.current_path()->copy();
    long long record_cost = cost;

    int iter_without_improvement = 0;

    int delta_cost = 0;
    double probability = 0;

    // main loop, temperature is decreasing at each step
    for (;;)
    {
        if (time_is_out(time_limit))
        {
			logger().info() << "Time ran out:" << _timer.time_elapsed_s() << "s";
            break;
        }

        // nested loop, for each temperature do semi-random step in the neighborhood
        for ( int step = 0; step < plugin.iterations; ++step )
        {

            // calculate cost of next neighbor without actually generation it
            delta_cost = neighbors.get_delta_cost(_distance_matrix);

            probability = plugin.probability(delta_cost, cost, probability_level);
            double random = plugin.random(probability_level);

            if ( probability > random )
            {
                // get permutation for current neighbor
                neighbors.generate();
                cost += delta_cost;
                assert(cost > 0);

                if ( cost < record_cost )
                {
                    record_cost = cost;
                    slice_copy( neighbors.current_path(), record_path, 0, 0, _number_of_cities );
                    iter_without_improvement = 0;
                }
            }

            neighbors.advance();
        }

        probability_level = plugin.update_probability_level(probability_level);

        iter_without_improvement++;

        if (progress)
        {
            printf("\r                                                                                     \r"
                "Current: %lld / Best: %lld   T: %f -> %f", cost, record_cost, probability_level, plugin.final_probability_level);
        }

        // exit when temperature is below minimum
        if ( plugin.is_finished(probability_level) )
        {
            break;
        }
    }

    cost = record_cost;
    slice_copy( record_path, result, 0, 0, _number_of_cities );

    IntVector::dealloc( record_path );
    IntVector::dealloc( current_path );

    if (progress)
    {
        printf("\r                                                                                    \r");
        printf("Final cost: %lld\n", cost);
    }

    return 0;
}

template <typename T_Container>
int TSP::Solver<T_Container>::varying_nbh_search( IntVector* result, long long& cost, bool silent ) const
{
	int time_limit = get_time_constraints(_number_of_cities);
	bool progress = options().hasopt<bool>("progress") && options().getopt<bool>("progress");

    //Local search with varying neighborhoods.
    //Neighborhoods are either 2-opt, 3-opt, 4-opt or 5-opt.

    // you can't do 5-opt in problem of size 4 ...
    assert(_number_of_cities >= 5);

    bool skip_5opt = options_VNS().skip_5opt();

    IntVector* current_path = create_initial_path();
    cost = calc_cost( _distance_matrix, current_path );

    // init neighborhoods
    Nbh2optSym* nbh_2opt_sym = NULL;
    Nbh2optAsym* nbh_2opt_asym = NULL;

    switch (_type)
    {
    case Symmetric:
        nbh_2opt_sym = new Nbh2optSym(_number_of_cities);
        nbh_2opt_sym->set_current_path(current_path);
        break;
    case Asymmetric:
        nbh_2opt_asym = new Nbh2optAsym(_number_of_cities);
        nbh_2opt_asym->set_current_path(current_path);
        break;
    default:
        assert(0 && "_type != Symmetric or Asymmetric");
        break;
    }

    Nbh3opt* nbh_3opt = new Nbh3opt(_number_of_cities);
    nbh_3opt->set_current_path(current_path);

    Nbh4opt* nbh_4opt = new Nbh4opt(_number_of_cities);
    nbh_4opt->set_current_path(current_path);

    Nbh5opt* nbh_5opt = new Nbh5opt(_number_of_cities);
    nbh_5opt->set_current_path(current_path);

    VNS_Stage nbh_stage = VNS_Stage_2opt;

    bool improvement_found = false;
    bool finished = false;

    while (!finished)
    {
		if (time_is_out(time_limit))
        {
			logger().info() << "Time ran out:" << _timer.time_elapsed_s() << "s";
            break;
        }

        switch (nbh_stage)
        {
        case VNS_Stage_2opt:
            {
				if (!silent && progress)
                    printf("\r                                                                           \r"
                        "Cost %lld; 2-opt", cost);

                switch (_type)
                {
                case Symmetric:
                    improvement_found = varying_nbh_search_worker( nbh_2opt_sym, cost, /*stop_on_improvement=*/false );
                    break;
                case Asymmetric:
                    improvement_found = varying_nbh_search_worker( nbh_2opt_asym, cost, /*stop_on_improvement=*/false );
                    break;
                default:
                    assert(0 && "_type != Symmetric or Asymmetric");
                    break;
                }

                if (!improvement_found)
                    nbh_stage = VNS_Stage_3opt;
                else
                    assert(0);

                break;
            }
        case VNS_Stage_3opt:
            {
                if (!silent && progress)
					printf("\r                                                                           \r"
                        "Cost %lld; 3-opt", cost);

                improvement_found = varying_nbh_search_worker( nbh_3opt, cost, /*stop_on_improvement=*/true );

                if (improvement_found)
                    nbh_stage = VNS_Stage_2opt;
                else
                    nbh_stage = VNS_Stage_4opt;

                break;
            }
        case VNS_Stage_4opt:
            {
                if (!silent && progress)
					printf("\r                                                                           \r"
                        "Cost %lld; 4-opt", cost);

                improvement_found = varying_nbh_search_worker( nbh_4opt, cost, /*stop_on_improvement=*/true );

                if (improvement_found)
                    nbh_stage = VNS_Stage_2opt;
                else
                    if (skip_5opt)
                        finished = true;
                    else
                        nbh_stage = VNS_Stage_5opt;

                break;
            }
        case VNS_Stage_5opt:
            {
                if (!silent && progress)
					printf("\r                                                                           \r"
                        "Cost %lld; 5-opt", cost);

                improvement_found = varying_nbh_search_worker( nbh_5opt, cost, /*stop_on_improvement=*/true );

                if (improvement_found)
                    nbh_stage = VNS_Stage_2opt;
                else
                    finished = true;

                break;
            }
        default:
            assert(0);
        }
    }

    if (nbh_2opt_sym != NULL)
        delete nbh_2opt_sym;
    else
        delete nbh_2opt_asym;

    delete nbh_3opt;
    delete nbh_4opt;
    delete nbh_5opt;

    slice_copy( current_path, result, 0, 0, _number_of_cities );
    IntVector::dealloc( current_path );

    return 0;
}

template <typename T_Container>
template <typename T_Nbh>
bool TSP::Solver<T_Container>::varying_nbh_search_worker( T_Nbh nbh, long long& cost, bool stop_on_improvement ) const
{
    int delta_cost = 0;

    // while not all neighborhoods were checked
    for (;;)
    {
        delta_cost = nbh->get_delta_cost( _distance_matrix );

        // accept solution (less is better)
        if ( delta_cost < 0 )
        {
            nbh->generate();
            nbh->save_stop_position();
            cost += delta_cost;
            assert(cost > 0);
            if (stop_on_improvement)
                return true;
        }
        // all neighbors were checked
        else if ( nbh->is_finished() )
        {
            nbh->save_stop_position();
            return false;
        }

        nbh->advance();
    }
}

template <typename T_Container>
int TSP::Solver<T_Container>::iterated_local_search( IntVector* result, long long& cost )
{
	int time_limit = get_time_constraints(_number_of_cities);
	bool progress = options().hasopt<bool>("progress") && options().getopt<bool>("progress");

    //Iterated local search
    options().setopt<bool>("skip_5opt", true);

    IntVector* current_path = create_initial_path();
    cost = calc_cost( _distance_matrix, current_path );

    IntVector* aux_path = current_path->copy();
    long long aux_cost = cost;

    // keep track of best solution
    IntVector* record_path = current_path->copy();
    long long record_cost = cost;

    int iterations = options_ILS().iterations();

    logger().info() << "";

    for ( int i = 0; i < iterations; i++ )
    {
		if (time_is_out(time_limit))
        {
			logger().info() << "Time ran out:" << _timer.time_elapsed_s() << "s";
            break;
        }

        // run nested search
        set_initial_approximation(current_path);
        int ret_code = varying_nbh_search( aux_path, aux_cost, true );
        if (ret_code != 0)
            return ret_code;

        if ( aux_cost < record_cost )
        {
            slice_copy( aux_path, record_path, 0, 0, _number_of_cities );
            record_cost = aux_cost;
        }

        // accept new tour if its cost is not worse then 150% of current
        if ( abs( double( aux_cost ) / cost ) < 1.5 )
        {
            slice_copy( aux_path, current_path, 0, 0, _number_of_cities );
            cost = aux_cost;
        }

        int node_1 = 1 + ( rand() % _number_of_cities / 4 );
        int node_2 = node_1 + 1 + ( rand() % _number_of_cities / 4 );
        int node_3 = node_2 + 1 + ( rand() % _number_of_cities / 4 );
        // perturb current permutation with random double-bridge move
        Permutations::double_bridge(current_path, node_1, node_2, node_3);

		if (progress)
		{
			printf("\r                                                                           \r"
				"%lld   %d / %d", record_cost, i, iterations);
		}
    }

    cost = record_cost;
    slice_copy( record_path, result, 0, 0, _number_of_cities );
    IntVector::dealloc( record_path );
    IntVector::dealloc( current_path );

	if (progress)
	{
		printf("\r                                                                           \r");
		printf("Final cost: %lld\n", cost);
	}

    return 0;
}


template <typename T_Container>
int TSP::Solver<T_Container>::guided_local_search( IntVector* result, long long& cost )
{
	int time_limit = get_time_constraints(_number_of_cities);
    int lambda = 60;

	bool progress = options().hasopt<bool>("progress") && options().getopt<bool>("progress");

    //save original matrix
    IntMatrix* original_distances = _distance_matrix->copy();

    // init all penalties to 0
    IntMatrix* penalties = IntMatrix::alloc(_number_of_cities);
    penalties->set_to_zero();

    IntVector* current_path = create_initial_path();
    cost = calc_cost( _distance_matrix, current_path );

    // keep track of best solution
    IntVector* record_path = current_path->copy();
    long long record_cost = cost;

    const int iterations = 5000;

    for ( int outer_count = 0; outer_count < iterations; ++outer_count )
    {
		if (time_is_out(time_limit))
        {
			logger().info() << "Time ran out:" << _timer.time_elapsed_s() << "s";
            break;
        }

        // run nested search
        set_initial_approximation(current_path);
        int res = local_search(current_path, cost, true);
        UNUSED(res);
        assert(res == 0);
        cost = TSP::calc_cost(original_distances, current_path);

        // save record
        if ( cost < record_cost )
        {
            slice_copy(current_path, record_path, 0, 0, _number_of_cities);
            record_cost = cost;
        }

		if (progress)
		{
			printf("\r                                                                           \r"
				   "%lld   %d / %d", record_cost, outer_count, iterations);
		}

        // find max utility (edge that is the most significant in the solution)
        double max_util = 0;
        int max_i = 0;
        int max_j = 0;

        for ( unsigned int i = 0; i < current_path->size; ++i )
        {
            int j;

            if ( i < current_path->size - 1 )
                j = i + 1;
            else
                j = 0;

            double util = original_distances->at( current_path->at(i), current_path->at(j) ) /
                ( 1 + penalties->at( current_path->at(i), current_path->at(j) ) );

            if ( util > max_util )
            {
                max_util = util;
                max_i = current_path->at(i);
                max_j = current_path->at(j);
            }
        }

        // penalize edge with max utility
        *( penalties->pointer_at(max_i, max_j) ) += 1;

        if ( _type == Symmetric )
        {
            *( penalties->pointer_at(max_j, max_i) ) += 1;
        }

        // update costs with new penalties
        for ( unsigned int i = 0; i < original_distances->size; ++i )
            for ( unsigned int j = 0; j < original_distances->size; ++j )
                *( _distance_matrix->pointer_at(i, j) ) = original_distances->at(i, j) + lambda * penalties->at(i, j);
    }

    cost = record_cost;
    slice_copy( record_path, result, 0, 0, _number_of_cities );
    IntVector::dealloc( record_path );
    IntVector::dealloc( current_path );
    IntMatrix::dealloc( _distance_matrix );
    IntMatrix::dealloc( penalties );

    // revert to original distance matrix
    _distance_matrix = original_distances;

	if (progress)
	{
		printf("\r                                                                           \r");
		printf("Final cost: %lld\n", cost);
	}

    return 0;
}


namespace TSP
{
    template <>
    inline int Solver<IntMatrList>::guided_local_search( IntVector* , long long&  )
    {
        assert(0 && "IntMatrList with guided_local_search is not supported");
        return 1;
    }
};


template <typename T_Container>
template <typename T_Model>
bool TSP::Solver<T_Container>::populational_alg_template( IntVector* result, long long& cost, int steps )
{
    T_Model model( this, _distance_matrix );

    int steps_without_improvement = 0;
    cost = std::numeric_limits<long long>::max();

	bool progress = options().hasopt<bool>("progress") && options().getopt<bool>("progress");
	int time_limit = get_time_constraints(_number_of_cities);

    // if no improvements for 'steps' steps then finish
    for (;;)
    {
		if (time_is_out(time_limit))
        {
			logger().info() << "Time ran out:" << _timer.time_elapsed_s() << "s";
            break;
        }

        model.make_one_step();

        if ( cost > model.get_record_cost() )
        {
            steps_without_improvement = 0;
            cost = model.get_record_cost();
        }
        else
        {
            steps_without_improvement++;
        }

		if (progress)
		{
			printf("\r                                                                           \r"
				   "%lld   %d / %d", cost, steps_without_improvement, steps);
		}

        if ( steps_without_improvement > steps )
        {
            break;
        }
    }

    slice_copy( model.get_record(), result, 0, 0, _number_of_cities );
    cost = model.get_record_cost();

	if (progress)
	{
		printf("\r                                                                           \r");
		printf("Final cost: %lld\n", cost);
	}

    return 0;
}

template <typename T_Container>
int TSP::Solver<T_Container>::genetic_algorithm( IntVector* result, long long& cost )
{
    return populational_alg_template< GeneticModel<T_Container, MixedRecombinator> >(result, cost, 500);
}

template <typename T_Container>
int TSP::Solver<T_Container>::ant_colony( IntVector* result, long long& cost )
{
    return populational_alg_template< AntColony<T_Container> >(result, cost, 200);
}

template <typename T_Container>
int TSP::Solver<T_Container>::h_algorithm( IntVector* result, long long& cost )
{
    return populational_alg_template< GeneticModel<T_Container, HRecombinator> >(result, cost, 500);
}

template class TSP::Solver<IntMatrix>;
template class TSP::Solver<IntMatrList>;
