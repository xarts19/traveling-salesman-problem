IDIR=GCC/include
ODIR=GCC/obj
CC=g++
CFLAGS=-Wall -fPIC -O2 -I$(IDIR)

#LDIR=../lib
#LIBS=-lm

_DEPS = tsp_types.h
DEPS = $(patsubst %,$(IDIR)/%,$(_DEPS))

_OBJ = TSPNeighborhood.o TSPSolver.o TSPGenetic.o TSPInterface.o VTCriticalSectionLinux.o VTLogger.o
OBJ = $(patsubst %,$(ODIR)/%,$(_OBJ))

all: $(ODIR) GCC/TSP.so

$(ODIR):
	mkdir -p $@

GCC/TSP.so: $(OBJ)
	$(CC) -shared $(CFLAGS) -o $@ $^
	
$(ODIR)/%.o: %.cpp
	$(CC) -c $(CFLAGS) -o $@ $^

.PHONY: clean
clean:
	rm -f $(ODIR)/*.o *~ GCC/TSP.so $(INCDIR)/*~


