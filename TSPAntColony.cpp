#include "TSPAntColony.h"

#include "TSPSolver.h"

#include <algorithm>

using namespace TSP;

void Ant::reset(unsigned int city, unsigned int number_of_cities)
{
    cost = 0;
    path_.size = number_of_cities;
    *(path_.pointer_at(0)) = city;
}

template <typename T_DistancesType>
void Ant::run(WeightProxy<T_DistancesType> weights, const std::set<unsigned int>& cities)
{
    std::set<unsigned int> unvisited( cities );

    unvisited.erase( path_.at(0) );
    int memory_size = 1;

    while ( !unvisited.empty() )
    {
        int current_city = path_.at(memory_size - 1);
        float sum = 0;

        std::set<unsigned int>::iterator city;
        for ( city = unvisited.begin(); city != unvisited.end(); ++city )
        {
            sum += weights(current_city, *city);
        }

        float random = sum * ( rand() % 10000 ) / 10000.0f;
        int new_city = -1;
        float probability = 0;

        // calculate probabilities of going to each city and select city based on random number
        for ( city = unvisited.begin(); city != unvisited.end(); ++city )
        {
            probability += weights(current_city, *city) / sum;

            if ( random <= probability )
            {
                new_city = *city;
                break;
            }
        }

        assert(new_city != -1);

        *(path_.pointer_at(memory_size++)) = new_city;
        unvisited.erase( new_city );
        sum -= weights(current_city, new_city);
    }
}


template <typename T_DistancesType>
AntColony<T_DistancesType>::AntColony( Solver<T_DistancesType>* solver, const T_DistancesType* distances ) : 
        _number_of_cities(distances->size),
        _solver( solver ),
        _distances( distances ),
        cities(),
        ants(),
        number_of_ants(10/*_number_of_cities > 100 ? 100 : _number_of_cities*/),
        ant_pool(static_cast<unsigned int>(Ant::memory(_number_of_cities)), number_of_ants),
        pheromones(FloatMatrix::alloc(_number_of_cities)),
        weights(_distances, pheromones),
        record_path(IntVector::alloc(_number_of_cities)),
        step_with_no_improvement(0),

        evaporation(0.3f),
        pheromone_min(0.0001f),
        pheromone_max(1.0f),
        ants_leave_pheromone(true)
{
    // assign parameters

    // optimized away
    //pheromone_influence = 1;
    //heuristic_influence = 3;

    for (unsigned int i = 0; i < _number_of_cities; ++i)
        cities.insert(i);

    for ( unsigned int i = 0; i < number_of_ants; i++ )
        ants.push_back( ant_pool.alloc() );

    // init pheromones
    pheromones->fill(pheromone_min);

    // init first record solution
    random_path(_number_of_cities, record_path);
    record_cost = calc_cost( _distances, record_path );

}

template <typename T_DistancesType>
AntColony<T_DistancesType>::~AntColony()
{
    IntVector::dealloc(record_path);
    FloatMatrix::dealloc(pheromones);
}

template <typename T_DistancesType>
void AntColony<T_DistancesType>::make_one_step()
{
    create_new_population();
    launch_ants();
    update_pheromones();
    daemon_action();
    evaporate();
    ++step_with_no_improvement;
    if (step_with_no_improvement < 10)
        weights.set_beta(5);
    else if (step_with_no_improvement < 50)
        weights.set_beta(4);
    else if (step_with_no_improvement < 100)
        weights.set_beta(3);
    else
        weights.set_beta(2);
}

template <typename T_DistancesType>
void AntColony<T_DistancesType>::create_new_population()
{
    
    if (number_of_ants == _number_of_cities)
    {
        for ( unsigned int i = 0; i < number_of_ants; i++ )
        {
            ants[i]->reset(i, _number_of_cities);
        }
    }
    else
    {
        for ( unsigned int i = 0; i < number_of_ants; i++ )
        {
            ants[i]->reset( rand() % _number_of_cities, _number_of_cities );
        }
    }
}

template <typename T_DistancesType>
void AntColony<T_DistancesType>::launch_ants()
{
    for ( unsigned int i = 0; i < number_of_ants; i++ )
    {
        ants[i]->run( weights, cities );
    }
}

template <typename T_DistancesType>
void AntColony<T_DistancesType>::update_pheromones()
{
    unsigned int new_record_ant = static_cast<unsigned int>(-1);

    for ( unsigned int ant = 0; ant < ants.size(); ++ant )
    {
        IntVector* path = ants[ant]->path();
        long long cost = calc_cost( _distances, path );
        ants[ant]->cost = cost;

        if ( ants_leave_pheromone )
        {
            for ( unsigned int i = 1; i < _number_of_cities; i++ )
            {
                int a = path->at(i - 1);
                int b = path->at(i);
                *(pheromones->pointer_at( a, b )) += 0.01f * (float(record_cost) / cost);
            }
        }

        if ( cost < record_cost )
        {
            new_record_ant = ant;
        }
    }

    if (-1 != new_record_ant)
    {
        record_cost = ants[new_record_ant]->cost;
        slice_copy( ants[new_record_ant]->path(), record_path, 0, 0, record_path->size );
        step_with_no_improvement = 0;
    }
}

template <typename T_DistancesType>
void AntColony<T_DistancesType>::evaporate()
{
    for ( unsigned int i = 0; i < _number_of_cities; i++ )
    {
        for ( unsigned int j = 0; j < _number_of_cities; j++ )
        {
            float new_pheromone_level = ( 1 - evaporation ) * pheromones->at(i, j);

            if ( new_pheromone_level <= pheromone_min )
                *( pheromones->pointer_at(i, j) ) = pheromone_min;

            else if ( new_pheromone_level >= pheromone_max )
                *( pheromones->pointer_at(i, j) ) = pheromone_max;

            else
                *( pheromones->pointer_at(i, j) ) = new_pheromone_level;
        }
    }
}

template <typename T_DistancesType>
void AntColony<T_DistancesType>::daemon_action()
{
    // find best path from current population
    int best_ant = 0;
    long long best_cost = ants[0]->cost;

    for ( unsigned int i = 1; i < ants.size(); i++ )
    {
        if ( ants[i]->cost < best_cost )
        {
            best_ant = i;
            best_cost = ants[i]->cost;
        }
    }

    IntVector* path = IntVector::alloc(_number_of_cities);
    long long cost = -1;

    // apply local search to this best path
    _solver->set_initial_approximation( ants[best_ant]->path() );
    int ret = _solver->local_search( path, cost );
    UNUSED(ret);
    assert( ret == 0 );

    // save if good
    if (cost < record_cost)
    {
        record_cost = cost;
        slice_copy( path, record_path, 0, 0, record_path->size );
        step_with_no_improvement = 0;
    }
    
    // apply pheromone for this path
    for ( unsigned int i = 1; i < _number_of_cities; i++ )
        *( pheromones->pointer_at( path->at(i - 1), path->at(i) ) ) += 0.03f; // 30 / float( cost )
    
    IntVector::dealloc(path);
}

template class TSP::AntColony<IntMatrix>;
template class TSP::AntColony<IntMatrList>;

