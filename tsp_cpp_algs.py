from ctypes import CDLL, Structure, byref, c_uint, c_int, c_void_p, c_char_p, c_bool, c_double, c_longlong

from options import Algorithms

import os
if os.name == 'nt':
    tsplib_debug = CDLL("TSP/Debug/TSP.dll")
    tsplib_release = CDLL("TSP/Release/TSP.dll")
elif os.name == 'posix':
    tsplib_debug = CDLL("GCC/TSP.so")
    tsplib_release = CDLL("GCC/TSP.so")
else:
    assert(false and "OS not supported")


class Point(Structure):
    _fields_ = [("x", c_double),
                ("y", c_double)]

def T_IntMatrix(size):
    class IntMatrix(Structure):
        _fields_ = [("size", c_uint),
                    ("matrix", c_int * size * size )]
    return IntMatrix
    
def T_IntMatrList(size):
    class IntMatrList(Structure):
        _fields_ = [("size", c_uint),
                    ("matrix", Point * size )]
    return IntMatrList
    
def T_IntVector(size):
    class IntVector(Structure):
        _fields_ = [("size", c_uint),
                    ("vector", c_int * size )]
    return IntVector


def init_solver(tsplib, distances, isMatrix, edge_type, alg, memory):
    size = len(distances)

    if isMatrix:
        c_distances_matrix = (c_int * size * size)()
        for i in range(size):
            for j in range(size):
                c_distances_matrix[i][j] = distances[i][j]

        solver = tsplib.new_solver_matrix( size, byref( T_IntMatrix(size)(size, c_distances_matrix) ), alg, memory )
    else:
        c_distances_vect = (Point * size)()
        for i in xrange(size):
            c_distances_vect[i] = Point(distances[i][0], distances[i][1])
            
        solver = tsplib.new_solver_list( size, byref( T_IntMatrList(size)(size, c_distances_vect) ), alg, memory )

    if not solver:
        return solver
        
    if edge_type == "TSP":
        tsplib.set_edge_type_symmetric( solver )
    elif edge_type == "ATSP":
        tsplib.set_edge_type_asymmetric( solver )
    else:
        assert(False and "edge_type != TSP or ATSP")

    return solver

    
def run_algorithm(options, problem_type, distances, isMatrix, initial_solution_vector):
    if options.debug:
        tsplib = tsplib_debug
    else:
        tsplib = tsplib_release
        
    tsplib.set_int_option.argtypes = c_void_p, c_char_p, c_int
    
    size = len(distances)

    solver = init_solver(tsplib, distances, isMatrix, problem_type, Algorithms[options.method], options.memory)
    if not solver:
        return [], -1
        
    tsplib.enable_logging(solver, options.log_level)

    if initial_solution_vector:
        size = len(initial_solution_vector);
    
        c_init_solution = (c_int * size)()
        for i in range(size):
            c_init_solution[i] = initial_solution_vector[i]
            
        tsplib.set_initial_approximation(solver, byref( T_IntVector(size)(size, c_init_solution) ))
        
        
    if options.seed != None:
        tsplib.set_seed(solver, options.seed)
        
    tsplib.clear_options(solver)

    if options.progress_detailed:
        tsplib.set_bool_option(solver, "progress".encode('utf-8'), c_bool(True))
    
    tsplib.set_int_option(solver, "time_limit".encode('utf-8'), options.time)
    
    for key, val in options.custom.items():
        if isinstance(val, bool):
            tsplib.set_bool_option(solver, key.encode('utf-8'), c_bool(val))
        if isinstance(val, int):
            tsplib.set_int_option(solver, key.encode('utf-8'), val)
        if isinstance(val, float):
            tsplib.set_double_option(solver, key.encode('utf-8'), c_double(val))

    # Create structure for returned solution
    v = (c_int * size)()
    result = T_IntVector(size)(size, v)
    cost = c_longlong(-1)

    ret_code = tsplib.run_algorithm( solver, byref(result), byref(cost) )

    tsplib.delete_solver.argtypes = [ c_void_p ]
    tsplib.delete_solver( solver )
    
    if ret_code != 0:
        return [], -1
    
    return result.vector, cost.value


def guided_local_search():
    pass

def genetic_algorithm():
    pass

def ant_colony():
    pass
