#define TSP_EXPORTS
#include "tsp_types.h"
#include "TSPInterface.h"
#include "TSPSolver.h"
#include "TSPAntColony.h"

IntMatrix* list_to_matrix(const IntMatrList* coord_list)
{
    IntMatrix* matrix = IntMatrix::try_alloc(coord_list->size);
    if (matrix == NULL)
        return NULL;

    for (unsigned int i = 0; i < coord_list->size; ++i)
    {
        for (unsigned int j = 0; j < coord_list->size; ++j)
        {
            matrix->to(i, j) = coord_list->at(i, j);
        }
    }
    return matrix;
}

TSP_API void* new_solver_matrix( unsigned int number_of_cities, const IntMatrix* distance_matrix, Algorithms alg, int memory_limit_mb )
{
    long long needed = IntMatrix::memory_for_matrix(number_of_cities);
    if (needed > 1024ll * 1024ll * memory_limit_mb)
    {
		printf("Not enough memory for matrix: memory_limit: %d MB; required: %d MB\n", memory_limit_mb, needed / (1024 * 1024));
    }

    IntMatrix* matr_copy = distance_matrix->copy();
    return static_cast<void*>( new TSP::Solver<IntMatrix>( number_of_cities, matr_copy, TSP::FullMatrix, alg ) );

}

TSP_API void* new_solver_list( unsigned int number_of_cities, const IntMatrList* coord_list, Algorithms alg, int memory_limit_mb )
{
    bool use_matrix = true;
    
    if (alg == AntColonyOptimization)
    {
        long long needed_for_matrix = IntMatrix::memory_for_matrix(number_of_cities);
        long long needed_for_list = IntMatrList::memory_for_matrix(number_of_cities);
        long long needed_for_pheromones = TSP::AntColony<IntMatrix>::memory_for_pheromones(number_of_cities);

        // distances matrix + pheromones fit
        if (needed_for_matrix + needed_for_pheromones < 1024ll * 1024ll * memory_limit_mb)
        {
           use_matrix = true;
        }
        // distances list + pheromones fit
        else if (needed_for_list + needed_for_pheromones < 1024ll * 1024ll * memory_limit_mb)
        {
            use_matrix = false;
        }
        // nothing fits
        else
        {
            printf("Not enough memory for distances and pheromones:"
                "   Memory limit: %d MB;\n"
                "   Required:     %d MB for pheromones;\n"
                "                 %d MB for matrix of distances\n"
                "             (or %d MB for list of distances)\n", 
                memory_limit_mb, (int)(needed_for_pheromones / (1024 * 1024)), (int)(needed_for_matrix / (1024 * 1024)), (int)(needed_for_list / (1024 * 1024)));
            return NULL;
        }
    }
    else
    {
        long long needed = IntMatrix::memory_for_matrix(number_of_cities);
        if (needed > 1024ll * 1024ll * memory_limit_mb)
        {
            use_matrix = false;
        }
    }

    IntMatrix* matr = NULL;
    if (use_matrix)
    {
        matr = list_to_matrix(coord_list);
    }

    if (use_matrix && matr)   // if system had enough memory to allocate matrix
        return static_cast<void*>( new TSP::Solver<IntMatrix>( number_of_cities, matr, TSP::FullMatrix, alg ) );
    else
    {
        IntMatrList* matr_list = coord_list->copy();
        return static_cast<void*>( new TSP::Solver<IntMatrList>( number_of_cities, matr_list, TSP::AdjList, alg ) );
    }
}

TSP_API void delete_solver( void* solver )
{
    switch ( static_cast<TSP::Solver<IntMatrix>*>(solver)->container_type )
    {
    case TSP::FullMatrix:
        delete static_cast<TSP::Solver<IntMatrix>*>( solver );
        break;
    case TSP::AdjList:
        delete static_cast<TSP::Solver<IntMatrList>*>( solver );
        break;
    default:
        assert(0 && "Solver->container_type != TSP::FullMatrix or TSP::AdjList");
        break;
    }
}

TSP_API void enable_logging( void* solver, LogLevel level )
{
    // matrix type don't matter for this call, so cast to whatever...
	auto s = static_cast<TSP::Solver<IntMatrix>*>(solver);

	switch (level)
    {
	case Debug:     s->set_logging(VT::LL_Debug);      break;
	case Info:      s->set_logging(VT::LL_Info);       break;
	case Warning:   s->set_logging(VT::LL_Warning);    break;
	case Error:     s->set_logging(VT::LL_Error);      break;
	case Critical:  s->set_logging(VT::LL_Critical);   break;
    }
}

TSP_API void disable_logging( void* solver )
{
    // matrix type don't matter for this call, so cast to whatever...

    static_cast<TSP::Solver<IntMatrix>*>( solver )->set_logging( VT::LL_NoLogging );
}

TSP_API void set_seed( void* solver, int seed )
{
    // matrix type don't matter for this call, so cast to whatever...

    static_cast<TSP::Solver<IntMatrix>*>( solver )->set_seed( seed );
}

TSP_API void set_edge_type_symmetric( void* solver )
{
    // matrix type don't matter for this call, so cast to whatever...

    static_cast<TSP::Solver<IntMatrix>*>( solver )->set_edge_type( TSP::Symmetric );
}

TSP_API void set_edge_type_asymmetric( void* solver )
{
    // matrix type don't matter for this call, so cast to whatever...

    static_cast<TSP::Solver<IntMatrix>*>( solver )->set_edge_type( TSP::Asymmetric );
}

#include <Windows.h>

TSP_API void set_int_option( void* solver, const char* name, int option )
{
    // matrix type don't matter for this call, so cast to whatever...
    Sleep(3000);
    static_cast<TSP::Solver<IntMatrix>*>( solver )->options().setopt<int>(name, option);
}

TSP_API void set_double_option( void* solver, const char* name, double option )
{
    // matrix type don't matter for this call, so cast to whatever...

    static_cast<TSP::Solver<IntMatrix>*>( solver )->options().setopt<double>(name, option);
}

TSP_API void set_bool_option( void* solver, const char* name, int option )
{
    // matrix type don't matter for this call, so cast to whatever...
    assert((option == 1 || option == 0) && "Bool option should be equal 0 or 1");

    static_cast<TSP::Solver<IntMatrix>*>( solver )->options().setopt<bool>(name, (option != 0));
}

TSP_API void clear_options( void* solver )
{
    // matrix type don't matter for this call, so cast to whatever...

    static_cast<TSP::Solver<IntMatrix>*>( solver )->options().clear();
}

TSP_API void set_initial_approximation( void* solver, const IntVector* path )
{
    // matrix type don't matter for this call, so cast to whatever...

    static_cast<TSP::Solver<IntMatrix>*>( solver )->set_initial_approximation( path );
}

TSP_API void disable_init_approx( void* solver )
{
    // matrix type don't matter for this call, so cast to whatever...

    static_cast<TSP::Solver<IntMatrix>*>( solver )->disable_init_approx();
}

template <typename T_Container>
int helper_select_algorithm( TSP::Solver<T_Container>* solver, Algorithms alg, IntVector* result, long long& cost )
{
    try
    {
        switch (alg)
        {
        case LocalSearch:               return solver->local_search( result, cost );
        case SimulatedAnnealing:        return solver->simulated_annealing( result, cost );
        case VaryingNeighborhoodSearch: return solver->varying_nbh_search( result, cost );
        case IteratedLocalSearch:       return solver->iterated_local_search( result, cost );
        case GuidedLocalSearch:         return solver->guided_local_search( result, cost );
        case GeneticAlgorithm:          return solver->genetic_algorithm( result, cost );
        case AntColonyOptimization:     return solver->ant_colony( result, cost );
        case GSAlgorithm:               return solver->gs_algorithm( result, cost );
        case HAlgorithm:                return solver->h_algorithm( result, cost );
        default:
            printf("Alg: %d\n", alg);
            assert(0 && "Wrong 'alg' argument");
            return 1;
        }
    }
    catch (std::exception&)
    {
        printf("Unknown error\n");
        return 1;
    }
}

TSP_API int run_algorithm( void* solver, IntVector* result, long long& cost )
{
    TSP::Solver<IntMatrix>* solv = static_cast<TSP::Solver<IntMatrix>*>(solver);
    solv->start_timer();
    switch ( solv->container_type )
    {
    case TSP::FullMatrix:
        return helper_select_algorithm<IntMatrix>( static_cast<TSP::Solver<IntMatrix>*>( solver ), solv->algorithm, result, cost );
    case TSP::AdjList:
        if (!solv->is_symmetric())
        {
			solv->logger().critical() << "Adjacency lists and assymetric tsps are incompatible.";
            return 1;
        }
        if (solv->algorithm == GuidedLocalSearch)
        {
			solv->logger().critical() << "Guided search doesn't support distances as adjacency list.";
            return 1;
        }
        return helper_select_algorithm<IntMatrList>( static_cast<TSP::Solver<IntMatrList>*>( solver ), solv->algorithm, result, cost );
    default:
        {
        assert(0 && "Solver->container_type != TSP::FullMatrix or TSP::AdjList");
        return 1;
        }
    }
}
