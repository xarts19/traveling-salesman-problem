#pragma once

#include "tsp_types.h"
#include "VTUtils/VTAllocator.h"

#include <set>
#include <vector>

#include <math.h>

namespace TSP
{
    template <typename T_Container>
    class Solver;

    template <typename T_DistancesType>
    class WeightProxy
    {
    public:
        WeightProxy() : _distances(NULL), _pheromones(NULL) {}
        WeightProxy(const T_DistancesType* distances, const FloatMatrix* pheromones) :
          _distances(distances), _pheromones(pheromones), _beta(2)
        {}

        void set_beta(int new_beta) { _beta = new_beta; }

        inline float operator () (unsigned int i, unsigned int j) const
        {
            /*
                for optimization I use specific case of pheromone_influence == 1 and heuristic_influence == 3
                also heuristic is assumed to be 1.0 / ( distances[i][j] + 1 )
                so instead of
                weight = pow( ( double )pheromones[i][j], pheromone_influence ) *
                         pow( ( double )heuristics[i][j], heuristic_influence );
                we have
            */
            float heuristic = 1.0f / ( _distances->at(i, j) + 1 );

            return _pheromones->at(i, j) * pow(heuristic, _beta);
        }

    private:
        const T_DistancesType* _distances;
        const FloatMatrix* _pheromones;
        int _beta;
    };

    class Ant {

    public:
        template <typename T_DistancesType>
        void run(WeightProxy<T_DistancesType> weights, const std::set<unsigned int>& cities);

        void reset(unsigned int city, unsigned int number_of_cities);

        inline IntVector* path() { return &path_; };

        static long long memory(unsigned int size) { return IntVector::memory(size) + sizeof(long long); }

    private:
        Ant();
        Ant(const Ant& other);
        Ant& operator = (Ant other);

    public:
        long long cost;

    private:
        IntVector path_;
    };

    template <typename T_DistancesType>
    class AntColony {

    public:
        AntColony(Solver<T_DistancesType>* solver, const T_DistancesType* distances);
        ~AntColony();

        void        make_one_step();
        IntVector*  get_record() const      { return record_path; }
        long long   get_record_cost() const { return record_cost; };

        static long long memory_for_pheromones(int number_of_cities)
        {
            return ( static_cast<long long>(number_of_cities) *
                     static_cast<long long>(number_of_cities) * sizeof(float) );
        }

    private:

        void create_new_population();
        void launch_ants();
        void update_pheromones();
        void evaporate();
        void daemon_action();

    private:
        unsigned int _number_of_cities;
        Solver<T_DistancesType>* _solver;
        const T_DistancesType* _distances;
        std::set<unsigned int> cities;
        std::vector<Ant*> ants;
        unsigned int number_of_ants;
        VT::Allocator<Ant> ant_pool;
        
        FloatMatrix* pheromones;
        WeightProxy<T_DistancesType> weights;

        IntVector* record_path;
        long long record_cost;
        int step_with_no_improvement;

        float pheromone_influence;     // alpha
        float heuristic_influence;     // beta
        float evaporation;
        float pheromone_min;
        float pheromone_max;
        bool ants_leave_pheromone;
    };
};
