#!/usr/bin/env python
# encoding: utf-8

"""Add coloring to default logging module."""

import logging


class ColoredStreamHandler(logging.StreamHandler):

    def __init__(self):
        logging.StreamHandler.__init__(self)

    def emit(self, record):
        levelno = record.levelno
        if(levelno>=50):
            color = '\x1b[31m' # red
        elif(levelno>=40):
            color = '\x1b[31m' # red
        elif(levelno>=30):
            color = '\x1b[33m' # yellow
        elif(levelno>=20):
            color = '\x1b[32m' # green 
        elif(levelno>=10):
            color = '\x1b[36m' # pink
        else:
            color = '\x1b[0m' # normal
        orig_msg = record.msg
        record.msg = color + record.msg +  '\x1b[0m'  # normal
        logging.StreamHandler.emit(self, record)
        record.msg = orig_msg


# now we patch Python code to add color support to logging.StreamHandler
def add_coloring_to_emit_windows(fn):
        # add methods we need to the class

    def new(*args):
        FOREGROUND_BLUE      = 0x0001 # text color contains blue.
        FOREGROUND_GREEN     = 0x0002 # text color contains green.
        FOREGROUND_RED       = 0x0004 # text color contains red.
        FOREGROUND_INTENSITY = 0x0008 # text color is intensified.
        FOREGROUND_WHITE     = FOREGROUND_BLUE|FOREGROUND_GREEN |FOREGROUND_RED
       # winbase.h
        STD_INPUT_HANDLE = -10
        STD_OUTPUT_HANDLE = -11
        STD_ERROR_HANDLE = -12

        # wincon.h
        FOREGROUND_BLACK     = 0x0000
        FOREGROUND_BLUE      = 0x0001
        FOREGROUND_GREEN     = 0x0002
        FOREGROUND_CYAN      = 0x0003
        FOREGROUND_RED       = 0x0004
        FOREGROUND_MAGENTA   = 0x0005
        FOREGROUND_YELLOW    = 0x0006
        FOREGROUND_GREY      = 0x0007
        FOREGROUND_INTENSITY = 0x0008 # foreground color is intensified.

        BACKGROUND_BLACK     = 0x0000
        BACKGROUND_BLUE      = 0x0010
        BACKGROUND_GREEN     = 0x0020
        BACKGROUND_CYAN      = 0x0030
        BACKGROUND_RED       = 0x0040
        BACKGROUND_MAGENTA   = 0x0050
        BACKGROUND_YELLOW    = 0x0060
        BACKGROUND_GREY      = 0x0070
        BACKGROUND_INTENSITY = 0x0080 # background color is intensified.     

        levelno = args[1].levelno
        if(levelno>=50):
            color = BACKGROUND_YELLOW | FOREGROUND_RED | FOREGROUND_INTENSITY | BACKGROUND_INTENSITY 
        elif(levelno>=40):
            color = FOREGROUND_RED | FOREGROUND_INTENSITY
        elif(levelno>=30):
            color = FOREGROUND_YELLOW | FOREGROUND_INTENSITY
        elif(levelno>=20):
            color = FOREGROUND_GREEN
        elif(levelno>=10):
            color = FOREGROUND_MAGENTA
        else:
            color =  FOREGROUND_WHITE
        
        import ctypes
        from ctypes.wintypes import ULONG, DWORD, _COORD, SMALL_RECT, BOOL, WORD
        class CONSOLE_SCREEN_BUFFER_INFOEX(ctypes.Structure):
            _fields_ = [("cbSize", ULONG),
                        ("dwSize", _COORD),
                        ("dwCursorPosition", _COORD),
                        ("wAttributes", WORD),
                        ("srWindow", SMALL_RECT),
                        ("dwMaximumWindowSize", _COORD),
                        ("wPopupAttributes", WORD),
                        ("bFullscreenSupported", BOOL),
                        ("ColorTable", DWORD*16)]

        # Constants from the Windows API
        STD_OUTPUT_HANDLE = -11
        hdl = ctypes.windll.kernel32.GetStdHandle(STD_OUTPUT_HANDLE)
        
        p_info = CONSOLE_SCREEN_BUFFER_INFOEX()
        p_info.cbSize = 96
        ctypes.windll.kernel32.GetConsoleScreenBufferInfoEx(hdl, ctypes.byref(p_info))
        
        ctypes.windll.kernel32.SetConsoleTextAttribute(hdl, color)
        
        ret = fn(*args)
        
        ctypes.windll.kernel32.SetConsoleTextAttribute(hdl, p_info.wAttributes)
        #ctypes.windll.kernel32.SetConsoleScreenBufferInfoEx(hdl, ctypes.byref(p_info))
        #print "after"
        return ret
    
    return new


import platform
if platform.system()=='Windows':
    # Windows does not support ANSI escapes and we are using API calls to set the console color
    ColoredStreamHandler.emit = add_coloring_to_emit_windows(logging.StreamHandler.emit)



